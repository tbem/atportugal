Atportugal::Application.routes.draw do
  root :to => "home#index"
  
  
  match 'restaurants/advanced_search' => "restaurants#advanced_search"
  match '/restaurants/search/:district' => "restaurants#search"
  
  match '/users/save_rest_prefs' => "users#save_rest_prefs"
  match '/users/setpass' => "users#setpass"
  match '/users/resetpassword' => "users#resetpassword"
  match 'users/sendpassemail' => "users#sendpassemail"
  match 'users/resetpass' => "users#resetpass"
  match 'users/changepass' => "users#changepass"
  match 'users/editprofile' => "users#editprofile"
  match 'users/saveprofile' => "users#saveprofile"
  match 'users/updateaccount' => "users#updateaccount"
  match 'users/myprofile' => "users#myprofile"
  match 'users/contents' => "users#contents"
  match 'search/simplesearch' => "search#simplesearch"
  match 'search/advancedsearch' => "search#advancedsearch"
  
  match '/login' => "sessions#new", :as => "login"
  match '/logout' => "sessions#destroy", :as => "logout"
  resource :users
  #resources :users
  resource :session
  resource :home
  resource :search
  resources :restaurant_ratings
  resources :restaurant_comments
  resources :restaurants
end
