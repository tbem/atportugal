set :application, "atportugal"
default_run_options[:pty] = true
#num servidor partilhado devo meter a false o :use_sudo
set :use_sudo, true
set :port, 50
set :user, "tiago"
set :domain, "atportugal.sytes.net"
set :runner, "tiago"

set :repository,  "git://github.com/spacelement/atportugal"
#set :template_dir, "/home/tiago/public_html/#{application}"
set :scm, :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`


server "atportugal.sytes.net", :web, :app, :db, :primary => true
set :deploy_to, "/home/tiago/public_html/#{application}"

ssh_options[:forward_agent] = true
set :branch, "master"

set :deploy_via, :copy

#role :web, ""                          # Your HTTP server, Apache/etc
#role :app, "your app-server here"                          # This may be the same as your `Web` server
#role :db,  "your primary db-server here", :primary => true # This is where Rails migrations will run
#role :db,  "your slave db-server here"

# If you are using Passenger mod_rails uncomment this:
# if you're still using the script/reapear helper you will need
# these http://github.com/rails/irs_process_scripts

namespace :deploy do 
  task :start, :roles => :app do 
    run "touch #{current_release}/tmp/restart.txt" 
  end 
 
  task :stop, :roles => :app do 
    # Do nothing. 
  end 

  desc "Restart Application" 
  task :restart, :roles => :app do 
    run "touch #{current_release}/tmp/restart.txt" 
  end 
end

#ISTO É a receita para os linkar o database.yml  e a pasta de uploads

namespace(:customs) do
  task :config, :roles => :app do
    run <<-CMD
      ln -nfs /home/tiago/public_html/atportugal/config/database.yml #{release_path}/config/database.yml
    CMD
  end
  task :symlink, :roles => :app do
    run <<-CMD
      ln -nfs /home/tiago/public_html/atportugal/public/system/ #{release_path}/public/system && 
      ln -nfs /home/tiago/public_html/atportugal/log #{release_path}/log
    CMD
  end
end

#The next line tells Capistrano that after it has performed the update_code methods, which updates the Rails application, 
#that it should symlink to our shared database.yml file so that Rails has the production database configuration settings.
after "deploy:update_code", "customs:config"
#The next line tells Capistrano that once it has finished symlinking the application (a normal deploy involves symlinking to the current directory),
# it should then create the symlink for our uploads directory.
after "deploy:symlink","customs:symlink"
#Este aki é para limpar os releases menos os ultimos 5
#after "deploy", "deploy:cleanup"


after "deploy", "deploy:migrate"
