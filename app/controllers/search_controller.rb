class SearchController < ApplicationController
  def simplesearch
    @words = params['search_input']
  end
  def advancedsearch
    @restaurant_categories_type1 = RestaurantCategory.where("restaurant_category_type = ?", 1)
    @restaurant_categories_type2 = RestaurantCategory.where("restaurant_category_type = ?", 2)
  end
end
