class HomeController < ApplicationController

def index
  @user = User.new
  unless logged_in?
    session[:rest_sugestions] = User.random_sugestions
  end
  @restaurants_sugestions = Restaurant.find(session[:rest_sugestions].slice(0..3))
end
  
end
