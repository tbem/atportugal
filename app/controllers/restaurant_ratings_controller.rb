class RestaurantRatingsController < ApplicationController

  before_filter :authenticate

  def create
    @restaurant = Restaurant.find_by_id(params[:restaurant_id])
            
    @restaurant_rating = RestaurantRating.new(params[:restaurant_rating])
   
    @restaurant_rating.restaurant_id = @restaurant.id
    @restaurant_rating.user_id = current_user.id
    if @restaurant_rating.save
      respond_to do |format|
        format.html { redirect_to restaurant_path(@restaurant), :notice => "A sua votação foi gravada!" }
        format.js
      end
    end
  end
  def update

    @restaurant = Restaurant.find_by_id(params[:restaurant_id])
    #logger.debug "VALUE:" << params[:restaurant_rating][:value].to_s
    @restaurant_rating = current_user.restaurant_ratings.find_by_restaurant_id(@restaurant.id)
      if @restaurant_rating.update_attributes(params[:restaurant_rating])
        respond_to do |format|
          format.html { redirect_to restaurant_path(@restaurant), :notice => "A sua votação foi gravada!" }
          format.js
        end
      end
    
  end


end
        


