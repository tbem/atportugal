class RestaurantsController < ApplicationController

  def new
    @restaurant = Restaurant.new
    @restaurant_caracteristics = RestaurantCaracteristic.all
     
    #@restaurant_categories = RestaurantCategory.where("restaurant_category_type = ?", 1)
    @restaurant_categories_type1 = RestaurantCategory.where("restaurant_category_type = ?", 1)
    @restaurant_categories_type2 = RestaurantCategory.where("restaurant_category_type = ?", 2)
    5.times {@restaurant.restaurant_photos.build}
    @closed_days = ClosedDay.all
  end

  def edit
    @restaurant = Restaurant.find(params[:id])
    #@restaurant_categories = RestaurantCategory.all
    @restaurant_categories_type1 = RestaurantCategory.where("restaurant_category_type = ?", 1)
    @restaurant_categories_type2 = RestaurantCategory.where("restaurant_category_type = ?", 2)
    5.times{@restaurant.restaurant_photos.build}
    @closed_days = ClosedDay.all
  end

  def update
    params[:restaurant][:restaurant_category_ids] ||= []
    params[:restaurant][:closed_day_ids] ||= []
    
    restaurant = Restaurant.find(params[:id])
    restaurant.restaurant_categories.clear
    restaurant.restaurant_categories += RestaurantCategory.find(:all, :conditions => {:id => params[:restaurant][:restaurant_category_ids]})
    restaurant.closed_days.clear
    restaurant.closed_days += ClosedDay.find(:all, :conditions => {:id => params[:restaurant][:closed_day_ids]})
    #restaurant.restaurant_comments += RestaurantComment.find(:all, :conditions => {:id => params[:restaurant][:restaurant_comment_ids]})
    #logger.debug "IDSSSSSSSS:" << params[:restaurant][:restaurant_comment_ids].size
    restaurant.attributes = params[:restaurant]
    #params[:restaurant][:restaurant_comments][] = restaurant.restaurant_comments
    
   
    if  restaurant.save
      @restaurant = restaurant
      #@comments = restaurant.restaurant_comments;
      @comment = @restaurant.restaurant_comments.new
      @comments = restaurant.restaurant_comments.order("created_at DESC").page(params[:page]).per(5)
      render :action => :show, :notice=>'Restaurante actualizado com sucesso!!!'
         
    else
      @restaurant = Restaurant.find(params[:id])
      @restaurant_categories = RestaurantCategory.all
      render :action => :edit
    end
  end
  def index
     @restaurants_sugestions = Restaurant.find(session[:rest_sugestions])
    
  end

  def create
    @restaurant = Restaurant.new(params[:restaurant])
    @restaurant.attributes = params[:restaurant]
    @restaurant.restaurant_categories << RestaurantCategory.find(params[:restaurant_categories]) unless params[:restaurant_categories].nil?
    @restaurant.restaurant_comments = []
    u = current_user

    if(u != nil)
      @restaurant.created_by = u.email
    end
    
    if  @restaurant.save
      redirect_to @restaurant
    
      
    else
      @restaurant_categories = RestaurantCategory.all
      render :action => :new
    end
  end
    
  def show
    @restaurant = Restaurant.find(params[:id])
    
    if logged_in?

      categories = @restaurant.restaurant_categories
      categories.each { |cat|

        current_user.increment_click(cat.id, 1)
      }
    end
    @comments = @restaurant.restaurant_comments.order("created_at DESC").page(params[:page]).per(5)
    if logged_in?
      @comment = @restaurant.restaurant_comments.new
      
    end
  end
  def  search
    @restaurants = Restaurant.where("district = '#{params[:district]}'").page(params[:page]).per(5)
  end
  def  advanced_search

    @restaurants_results = Restaurant.advc_search(params).page(params[:page]).per(5)

    unless current_user.nil?
      categories  = []
      unless  params[:restaurant].nil_or_empty?
        unless  params[:restaurant][:restaurant_categories][:tipe_1_ids].nil_or_empty?
          categories = categories + params[:restaurant][:restaurant_categories][:tipe_1_ids]
        end
        unless  params[:restaurant][:restaurant_categories][:tipe_2_ids].nil_or_empty?
          categories = categories + params[:restaurant][:restaurant_categories][:tipe_2_ids]
        end
      end
      unless categories.empty?
        categories.each { |cat|

          current_user.increment_click(cat, 3)
        }
      end

    end
   
   
  end
    
end
