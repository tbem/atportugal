class RestaurantCommentsController < ApplicationController
  before_filter :authenticate

  def create
    
    restaurant = Restaurant.find_by_id(params[:restaurant_id])
    restaurant_comment = RestaurantComment.new(params[:restaurant_comment])

    restaurant_comment.restaurant_id = restaurant.id
    restaurant_comment.user_id = current_user.id
    if restaurant_comment.save
      respond_to do |format|
        format.html { redirect_to restaurant_path(restaurant), :notice => "A seu comentario foi gravado!" }
        format.js
      end
    end
  end
end
