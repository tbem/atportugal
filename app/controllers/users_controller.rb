require 'digest'
require 'time'
require 'date'
class UsersController < ApplicationController
  def new
    @user = User.new
    
  end

  def create
    user = User.new(params[:user])
    profile = Profile.new
    



    user_pre_preference = UserPrePreference.new
    
    user.profile = profile
    if user.save
      user_pre_preference.user_id = profile.user_id
      
      rest_categories = RestaurantCategory.all
      rest_categories.each { |cat|  
        pre_pref = UserPrePreferencesOfRestaurantCategory.new
        pre_pref.restaurant_category_id = cat.id
        pre_pref.user_pre_preference_id = user_pre_preference.id
        pre_pref.significance = 0
        user_pre_preference.user_pre_preferences_of_restaurant_categories << pre_pref
       }
        #cena para inicializar clicks das categorias de restaurantes
        rest_cats = RestaurantCategory.all
        rest_cats.each { |cat|

          cat_click = RestaurantCategoriesClick.new
          cat_click.user_id = user.id
          cat_click.restaurant_category_id = cat.id
          cat_click.number_of_clicks = 0
          cat_click.save
        }
    
      
      user_pre_preference.save

      respond_to do |format|
        format.html { redirect_to root_path, :notice=>'Utilizador criado com sucesso!!!' }
        format.js
      end
      
      
    else
      render :action =>'new'
    end
  end

  def edit
    @user = current_user

  end

  def updateaccount

    @user = current_user
    if(params[:user][:password] != nil && params[:user][:password] != "" && params[:user][:password] == params[:user][:password_confirmation])
      #logger.debug "okokokokokopass:" << params[:user][:password]
      if @user.update_attributes(params[:user])
        render :action => 'myprofile', :notice =>'Dados do utilizador atualizados com sucesso!'
      else
        if(request.referer == "/users/editprofile")
          render :action => 'editprofile'
        else
          render :action => 'edit'
        end
      end
    else
      params[:user].delete(:password)
      if(@user.update_attributes(params[:user]))
        render :action => 'myprofile', :notice =>'Dados do utilizador atualizados com sucesso!'
      else
        render :action => 'editprofile'
      end

    end
    
  end
  #this is just to run the JS associated
  def setpass
   
    
  end
  #Here we verify if user email submit exist in the database and procede with respective actions
  def sendpassemail
    @user = User.find_by_email(params[:email])

    #if the user exist, we create a new PasswordResetRequest record and send an email to the respective user if he doesn't exist we just process a JS to
    # show a error message.
    if(@user!=nil)
      
      o = PasswordResetRequest.new :user_email =>@user.email
      o.save;
      pass_rest = PasswordResetRequest.where(:user_email=>@user.email).order("created_at DESC").limit(1)
      a = pass_rest[0].id.to_s()
      a << @user.email
      hash = Digest::SHA1.hexdigest(a)

      #logger.debug "FDDDDDDDDDDXXXXXXXXXXXXXx"<< @a.inspect
      p = PasswordResetRequest.find(pass_rest[0].id)
      p.request_hash = hash
      p.save
      #@pass_rest.update_attributes :request_hash=>@hash
      Notifier.send_pass_reset(@user).deliver
    else
      render 'nonexist.js.erb'
    end
  end
  
  #This is just to verify if the user is valid, and the request for pass canche is up to date this comes from the link sent to user email
  def resetpass
    dif = nil
    @user = nil
    @valid_reset = false

    hash_request = params['key']
    p = PasswordResetRequest.find_by_request_hash(hash_request)
    #Get the hours that passed from the request time if this value is supirior to 12 hours we shoul'd let the user reset the pass   
    if(p != nil)
      dif = (Time.now - p.created_at.to_time)/1.day * 24
      @user = User.find_by_email(p.user_email)
    end
    #if the user doen't exist something goes wrong, may be someone trying to hack the pass reset providing another hash, so we can't let him go
    if(@user != nil && dif >= 12)
      @valid_reset = true
    end
    respond_to do |format|
      format.html 
      
    end
    
  end
  #this is the method to update the user password
  def changepass
   
    if(params[:password] == params[:password_confirmation])
      @user = User.find(params[:id])
      @user.password = params[:password]
      @user.save
      #logger.debug "EPA:FDX ESOTU AQUI!!!!!!!!!!!!!!!!!!!"
      respond_to do |format|
        format.html { redirect_to root_path, :notice=>'Dados actualizados com sucesso!!!' }
        format.js
      end
      
    else
      
      render 'passnotequal.js.erb'
    end
   
  end
  def editprofile
    @user = current_user

    p = current_user.user_pre_preference
    @rest_prefs_type1_array =[]
    rest_prefs_type1 = p.user_pre_preferences_of_restaurant_categories
    rest_prefs_type1.each { |cat|
      category = RestaurantCategory.find(cat.restaurant_category_id)
      if(category.restaurant_category_type == 1)
        cenas = {:name =>"#{category.name}", :value=>"#{cat.significance}", :id=>"#{category.id}"}
        @rest_prefs_type1_array << cenas
      end
    }
    @rest_prefs_type2_array =[]
    rest_prefs_type1 = p.user_pre_preferences_of_restaurant_categories
    rest_prefs_type1.each { |cat|
      category = RestaurantCategory.find(cat.restaurant_category_id)
      if(category.restaurant_category_type == 2)
        cenas = {:name =>"#{category.name}", :value=>"#{cat.significance}", :id=>"#{category.id}"}
        @rest_prefs_type2_array << cenas
      end
    }
   
    
  end
  def saveprofile
    
    profile = current_user.profile
    
    profile.attributes =  params[:profile]
    profile.save
    
    data = nil
    #logger.debug "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU:" << (params[:profile][:phone_number].to_s)
    if(params[:profile][:birth_date] != nil)
      data = Date.parse(params[:profile][:birth_date].to_s)
    end
    #user.profile.home_location= "lisboa"
    profile.birth_date = data
    @user = current_user
    @user.profile = profile

    t1 = @user.user_pre_preference.user_pre_preferences_of_restaurant_categories

    @type1 = []
    @type2 = []
    t1.each { |cat|

      c = RestaurantCategory.find(cat.restaurant_category_id)
      a = {:name=>c.name, :significance=>cat.significance}
      if c.restaurant_category_type == 1

        @type1 << a
      else
        @type2 << a
      end

    }
    
   
    if(profile.save)
      render :action=>'myprofile', :notice =>'Dados do utilizador atualizados com sucesso!'
    else
      render :action=>'editprofile', :notice=>'Alguma coisa correu mal!'
    end
  end
  def myprofile
    @user = current_user
    t1 = @user.user_pre_preference.user_pre_preferences_of_restaurant_categories

    @type1 = []
    @type2 = []
    t1.each { |cat|

      c = RestaurantCategory.find(cat.restaurant_category_id)
      a = {:name=>c.name, :significance=>cat.significance}
      if c.restaurant_category_type == 1
        
        @type1 << a
      else
        @type2 << a
      end

    }
    
    
  end
  def save_rest_prefs
    
    p = current_user.user_pre_preference
    p.restaurant_categories.clear
    p.user_pre_preferences_of_restaurant_categories.clear
    ids_and_values = params[:rest_pref_ids].split(",")
    
    ids_and_values.each { |cena|
      values = cena.split(":")

      e = UserPrePreferencesOfRestaurantCategory.new
      e.restaurant_category_id = values[0]
      e.user_pre_preference_id = p.id
      e.significance   = values[1]
      e.save


    }
    #r = RestaurantCategory.find(id)
    # p.restaurant_categories << r
      
    
    if p.save
      @user = current_user
    
      redirect_to '/users/myprofile',  :notice =>'Dados do utilizador atualizados com sucesso!'
    end
  end
end
