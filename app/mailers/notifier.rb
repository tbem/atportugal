class Notifier < ActionMailer::Base
  default :from => "tbem01@gmail.com"
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifier.send_pass_reset.subject
  #
  def welcome_user(user)
  @user = user
  end
  def send_pass_reset(user)
    @host_name = "localhost:3000"
     @user = user
     @p = PasswordResetRequest.where(:user_email=>@user.email).order("created_at DESC").limit(1)
     @hash_request = @p[0].request_hash
     mail :to => @user.email, :subject => "@Portugal pedido de mudança de password"

  end
end
