class RestaurantCaracteristic < ActiveRecord::Base
  has_many :restaurant_caracteristics_of_restaurants
  has_many :restaurants, :through => :restaurant_caracteristics_of_restaurant
  validates :name, :uniqueness => true
  
end
