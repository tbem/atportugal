require 'digest'
class User < ActiveRecord::Base
  has_and_belongs_to_many :categories
  has_one :profile
  has_one :user_pre_preference
  has_many :restaurant_comment
  has_many :restaurant_categories_click
  has_many :restaurant_categories, :through => :restaurant_categories_of_user
  has_many :event_categories, :through => :event_categories_of_users
  has_many :interess_point_categories, :through => :interess_point_categories_of_users
  has_many :hotel_categories, :through => :hotel_categories_of_users
  
  has_many :restaurant_ratings
  has_many :rated_restaurants, :through => :restaurant_ratings, :source => :restaurants

  before_create :encrypt_password
  before_update :encrypt_password
  validates :email, :uniqueness => true,
    :length => { :within => 5..50 },
    :format => { :with => /^[^@][\w.-]+@[\w.-]+[.][a-z]{2,4}$/i }
  validates :password, :confirmation => true,
    :length => { :within => 4..20 },
    :presence => true, :on =>"create"
  
  validates :name, :length => { :within => 3..20 },
    :presence => true
  validates :last_name, :length => { :within => 3..20 },
    :presence => true

  def self.authenticate(email, passwd)
    
    u = User.find_by_email email
    u && u.authenticated?(passwd) ? u : nil
  end
  #-----------------------------------------------------------------------------
  # Encrypts some data with the salt.
  def self.encrypt(passwd, salt)
    Digest::SHA1.hexdigest("--#{salt}--#{passwd}--")
  end
  #-----------------------------------------------------------------------------
  # Encrypts the password with the user salt
  def encrypt(passwd)
    self.class.encrypt(passwd, salt)
  end
  #-----------------------------------------------------------------------------
  def authenticated?(passwd)
    password == encrypt(passwd)
  end
  def complete_name
    self.name + " " + self.last_name
  end

  def total_number_of_gastronomy_clicks
    nr_clicks = 0
    array_categories_clicks  = self.restaurant_categories_click
    
    array_categories_clicks.each { |cat| 
      c = RestaurantCategory.find(cat.restaurant_category_id)
      if c.restaurant_category_type == 1
        nr_clicks = nr_clicks + cat.number_of_clicks
      end
      
    }
    nr_clicks
  end
  def total_number_of_price_clicks
    nr_clicks = 0
    array_categories_clicks  = self.restaurant_categories_click

    array_categories_clicks.each { |cat|
      c = RestaurantCategory.find(cat.restaurant_category_id)
      if c.restaurant_category_type == 2
        nr_clicks = nr_clicks + cat.number_of_clicks
      end

    }
    nr_clicks
  end
  def user_pref_category_value(category_id)
    p = self.user_pre_preference
    pref = p.user_pre_preferences_of_restaurant_categories.find_by_restaurant_category_id(category_id)
   
    pref.significance.to_f
  end
  #TODO MELHOR ISTO FAZER MENOS QUERYS
  def user_pref_categories_hash(type)
   
    hash_cats = Hash.new
    cats = RestaurantCategory.all(:select => "id, restaurant_category_type")

    cats.each { |cat|
      if(cat.restaurant_category_type == type)
        hash_cats.store(cat.id, user_pref_category_value(cat.id))
      end
     
    }
    
    hash_cats
  end

  
  #OPTIMO DEMORA MENOS QUE 1 seg a construir a hash ja com tudo calculado
  def category_clicks_hash(type)
    
    hash_clicks = Hash.new
    cats = RestaurantCategory.all(:select => "id, restaurant_category_type")
    
    ids =[]
    cats.each { |cat|
      if(cat.restaurant_category_type == type)
        ids << cat.id
      end
    }
     
    avgs = percent_of_clicks_by_user(ids)
    
    avgs.each { |avg|
      hash_clicks.store(avg.restaurant_category_id, avg.click_avg.to_f)

    }
    
    
    hash_clicks
  end
    
  def fuzzy(type)
    hash_fuzzy_results = Hash.new
    cats = RestaurantCategory.all(:select => "id, restaurant_category_type")
    user_prefs_hash = user_pref_categories_hash(type)
    click_hash = category_clicks_hash(type)
    #logger.debug "-----------------------------------INCIO PROLOG" << Time.now.to_s
    cats.each { |cat|

      if(cat.restaurant_category_type == type)
        u = user_prefs_hash[cat.id].to_f
        if(u.is_int?)
          u = u.to_i
        end
        c = click_hash[cat.id].to_f
        if(c.is_int?)
          c = c.to_i
        end
        html = Net::HTTP.get(URI.parse("http://192.168.0.12:4000/?user_pref=#{u}&click_avg=#{c}"))
        #html = Net::HTTP.get(URI.parse("http://prolog.sytes.net/?user_pref=#{u}&click_avg=#{c}"))
        value = html.scan(/<h2>(.*)<\/h2>/).flatten
       
        hash_fuzzy_results.store(cat.id, value[0].to_f)
      end
      
    }
    #logger.debug "-----------------------------------FIM PROLOG" << Time.now.to_s
    hash_fuzzy_results
    
  end
  def percent_of_clicks_by_user(categories)
    user_clicks_avg = RestaurantCategoriesClick.find_by_sql(" select restaurant_category_id, number_of_clicks / (select sum(number_of_clicks) from restaurant_categories_clicks where
  user_id = #{self.id} and restaurant_category_id in (#{categories.join(",")})) as click_avg  from restaurant_categories_clicks where user_id = #{self.id} and restaurant_category_id in (#{categories.join(",")})")
    b =  user_clicks_avg.sort { |x,y| y.click_avg <=> x.click_avg}
    b
   
  end
  #Return the percent_os clicks for a given category
  def percent_of_click_of_category(category_id, type)
    
    #rest_cat = RestaurantCategory.find(category_id)
    s = RestaurantCategoriesClick.where("restaurant_category_id = #{category_id} and user_id = #{self.id}")
    clicks_number = s[0].number_of_clicks
    if clicks_number != 0
      if type == 1
        if total_number_of_gastronomy_clicks != 0
          clicks_number.to_f/total_number_of_gastronomy_clicks.to_f
        else
          0
        end
      else
        if total_number_of_price_clicks != 0
          
          (clicks_number.to_f/total_number_of_price_clicks.to_f)
        else
          0
        end
      end
    else
      0
    end
  end
  #increment the number of clicks in a category by the given vaule to increment
  def increment_click(category_id, increment)
    c = RestaurantCategoriesClick.where("restaurant_category_id = ? and user_id = ?", category_id, self.id)
    
    new_number = c[0].number_of_clicks + increment
    c[0].number_of_clicks = new_number
    c[0].save
  end
  #Devolve o modo em que serão feitas as sugestões
  #1- Modo Aleatorio (Caso em que não existe 10 clicks nem a preferencias preenchidas)
  #2- Modo Preferencias de user (caso em que não existe 10 clicks mas o utilizador já configurou algumas das preferencias)
  #3- Modo Tracking (Caso em que o user já tem 10 cliks mas ainda não preencheu as suas preferencias)
  #4- Modo Fuzzy(Caso em que é necessário recorrer ao fuzzis para obter as preferencias)
  def self.sugestions_mode(current_user)
    modo = 1 #Default modo aleatorio
    nr_clikcs = 0
    user_prefs = false
    unless (current_user.nil?)
      ids_type1 = RestaurantCategory.select("id").where("restaurant_category_type=1")
      nr_clikcs =  current_user.restaurant_categories_click.sum(:number_of_clicks, :conditions=>"restaurant_category_id in (#{(ids_type1.map &:id).join(",")})").to_i
    end
    unless (current_user.nil?)
      nr = current_user.user_pre_preference.user_pre_preferences_of_restaurant_categories.sum(:significance).to_i
    end
    unless(nr == 0)
      user_prefs = true
    end
    if(nr_clikcs < 10 && user_prefs == true)
      modo = 2
    end
    if(nr_clikcs >= 10 && user_prefs == false)
      modo = 3
    end
    if(nr_clikcs >= 10 && user_prefs == true)
      modo = 4
    end
    logger.info "MODO DE SUGESTOES----->" << modo.to_s
    modo
    
  end

  #returns a hash with category_id_of_type1 => number_of_restaurants
  def number_of_restaurants_per_category_type1
    restaurant_count_hash = Restaurant.joins(:restaurant_categories).where("restaurant_category_type=1").group("restaurant_category_id").count()
    restaurant_count_hash
  end
  #Return the number of restaurants for each type2 category for a given type1_category_id
  def number_of_restaurants_per_category_type2(type_1_id)
    restaurant_count_hash =  Restaurant.count(:include => [:restaurant_categories], :joins=> " inner join restaurant_categories_of_restaurants rcor on rcor.restaurant_id = restaurants.id and rcor.restaurant_category_id = #{type_1_id} ", :group => "restaurant_categories.id", :conditions => "restaurant_categories.id not in (select id from restaurant_categories where restaurant_category_type=1)" )
    restaurant_count_hash
  end
  #resturn an array of random restaurants
  def self.random_sugestions
    #restaurants_array = Restaurant.all(:conditions => "city = '#{city}'", :order=>"RAND()", :limit => 6)
    #restaurants_array = Restaurant.all(:order=>"RAND()", :limit => 6)
    restaurants_array = Restaurant.select('id').order("RAND()").limit(6)
    #if(restaurants_array.size < 6)
    # restaurants_array += Restaurant.all(:conditions => "NOT (city = '#{city}')", :order=>"RAND()", :limit => 6-restaurants_array.size)

    #end
    ids_array = []
    restaurants_array.each { |i|

      ids_array << i.id
    }
    ids_array
    
    
    
  end
  #When just user_preferences are seted
  def user_sugestions(district)
    
    significances_array_type1 = user_pre_preference.user_pre_preferences_of_restaurant_categories.select("restaurant_category_id, significance").joins(:restaurant_category).where("restaurant_category_type = 1").order("significance DESC, RAND()")
    significances_array_type2 = user_pre_preference.user_pre_preferences_of_restaurant_categories.select("restaurant_category_id, significance").joins(:restaurant_category).where("restaurant_category_type = 2").order("significance DESC, RAND()")
    selected_rests_ids = []
    
    significance_type1_array = significances_array_type1
    significance_hash = Hash.new
    significance_type1_array.each { |s|

      significance_hash.store(s.restaurant_category_id, (s.significance.to_f ))

    }
    #Check if a category have at least one restaurant for sugest if not, that category should't be added to the prefered cats
    rest_numbers_hash = number_of_restaurants_per_category_type1
    #Select the best categoreis for user of type1
    selected_categories = []
    while selected_categories.size < 6 do

      significance_hash.each { |key,value|

        random_num = rand(100).to_f
        if (random_num < (value *10)  and selected_categories.size < 6 and rest_numbers_hash[key] != nil and rest_numbers_hash[key] > 0)
          logger.info "--------Rand-------->" << random_num.to_s << ".........Significance------->" << (value*10).to_s
          logger.info "--------CAT ID-------->" << key.to_s
          selected_categories << key
          #Isto aqui é para garantir que ele só repete uma categoria caso exista restaurantes suficientes para a poder repetir
          rest_numbers_hash[key] = rest_numbers_hash[key] - 1
        end

      }
    end
    #Categorias preferidas para os 6 rests
    #logger.debug "Categorias seleccionadas---->" << selected_categories.join("::")
    logger.info "Categorias seleccionadas---->" << significance_hash.to_yaml
    logger.info "Categorias seleccionadas---->" << selected_categories.join("::")
    #Ir buscar as categorias de preços
    
    significance_type2_array = significances_array_type2
    significance_type_2_hash = Hash.new
    significance_type2_array.each { |s|

      significance_type_2_hash.store(s.restaurant_category_id, (s.significance.to_f))

    }
    possible_rests_by_cat = []
    used_ids = []
    #vai Buscar os restaurantes que podem ser possíveis dentro das categorias de comida preferidas
    selected_categories.each { |item|
      #used_string_ids = used_ids.empty? ? 0 : used_ids.join(",")
      rests = Restaurant.joins(:restaurant_categories).select("restaurants.id").where("restaurant_category_id = #{item} and district='#{district}' ").limit(30).order("RAND()")

      #hash de cat_type1 id para array de rests dessa cat
      possible_rests_by_cat << [item, rests]

      rests.each { |i|

        used_ids << i.id
      }
    }

    
    while selected_rests_ids.size < 6 do
    
      #Depois de escolhidas as categorias do tipo 1 que vão ser recomendadas, vou fazer um array dos restaurantes para cada uma delas
      #para que quando a recomendação baseada na cat2 se poder basear apenas na lista de restaurantes de cat1
      selected_categories.each_with_index { |s_cat, index|
        rests_of_cat_ids = []

        #Isto é para fazer um array dos restaurantes que existem de cada categoria
        possible_rests_by_cat[index][1].each { |item|
          rests_of_cat_ids << item.id

        }
        #quando o gajo é inserido tenho k saltar fora senão ele vai inserir mais desta cat1 e quero passar para a próxima assim que ele apanha 1
        inserido = false
        significance_type_2_hash.each_pair { |key,value|
          random = rand(100)
          selected_rest = []

          #para garantir que não vão ser adicionados restaurantes repetidos
          ids_usados = selected_rests_ids.empty? ? 0 : selected_rests_ids.join(",")

          if(selected_rests_ids.size < 6 and inserido == false and random <=value)
            selected_rest = Restaurant.joins(:restaurant_categories_of_restaurant).select("id").where("restaurant_id not in (#{ids_usados}) and restaurant_id in (#{rests_of_cat_ids.join(",")}) and restaurant_category_id=#{key}").order("RAND()").limit(1)
            unless selected_rest.empty?
              puts s_cat.to_s
              selected_rests_ids << selected_rest[0].id
              inserido = true
              selected_categories.delete_at(index)
              possible_rests_by_cat.delete_at(index)
            end

          end


        }

      }


    end


    selected_rests_ids
  end
  #When user_preferences are not seted and nº of clicks is greater then 10
  def tracking_sugestions(district)
    selected_rests_ids = []
    categories_type1 = RestaurantCategory.all(:select =>"id", :conditions=>"restaurant_category_type=1")
    cat1_ids =[]
    categories_type1.each { |i|
      cat1_ids << i.id

    }
    significance_type1_array = percent_of_clicks_by_user(cat1_ids)
    significance_hash = Hash.new
    significance_type1_array.each { |s|

      significance_hash.store(s.restaurant_category_id, (s.click_avg.to_f * 10))

    }
    #Check if a category have at least one restaurant for sugest if not, that category should't be added to the prefered cats
    rest_numbers_hash = number_of_restaurants_per_category_type1
    #Select the best categoreis for user of type1
    selected_categories = []
    while selected_categories.size < 6 do
      
      significance_hash.each { |key,value|

        random_num = rand(100).to_f
        if (random_num < (value *10)  and selected_categories.size < 6 and rest_numbers_hash[key] != nil and rest_numbers_hash[key] > 0)
          logger.info "--------Rand-------->" << random_num.to_s << ".........Significance------->" << (value*10).to_s
          logger.info "--------CAT ID-------->" << key.to_s
          selected_categories << key
          #Isto aqui é para garantir que ele só repete uma categoria caso exista restaurantes suficientes para a poder repetir
          rest_numbers_hash[key] = rest_numbers_hash[key] - 1
        end

      }
    end
    #Categorias preferidas para os 6 rests
    logger.info "Categorias seleccionadas---->" << significance_hash.to_yaml
    logger.info "Categorias seleccionadas---->" << selected_categories.join("::")

    #Ir buscar as categorias de preços
    categories_type2 = RestaurantCategory.all(:select =>"id", :conditions=>"restaurant_category_type=2")
    cat2_ids =[]
    categories_type2.each { |i|
      cat2_ids << i.id

    }
    significance_type2_array = percent_of_clicks_by_user(cat2_ids)
    significance_type_2_hash = Hash.new
    significance_type2_array.each { |s|

      significance_type_2_hash.store(s.restaurant_category_id, (s.click_avg.to_f * 10))

    }
    possible_rests_by_cat = []
    used_ids = []
    #vai Buscar os restaurantes que podem ser possíveis dentro das categorias de comida preferidas
    selected_categories.each { |item|
      #used_string_ids = used_ids.empty? ? 0 : used_ids.join(",")
      rests = Restaurant.joins(:restaurant_categories).select("restaurants.id").where("restaurant_category_id = #{item} and district='#{district}' ").limit(30).order("RAND()")
      
      #hash de cat_type1 id para array de rests dessa cat
      possible_rests_by_cat << [item, rests]

      rests.each { |i|

        used_ids << i.id
      }
    }
   

    while selected_rests_ids.size < 6 do

      #Depois de escolhidas as categorias do tipo 1 que vão ser recomendadas, vou fazer um array dos restaurantes para cada uma delas
      #para que quando a recomendação baseada na cat2 se poder basear apenas na lista de restaurantes de cat1
      selected_categories.each_with_index { |s_cat, index|
        rests_of_cat_ids = []
        
        #Isto é para fazer um array dos restaurantes que existem de cada categoria
        possible_rests_by_cat[index][1].each { |item|
          rests_of_cat_ids << item.id
          
        }
        #quando o gajo é inserido tenho k saltar fora senão ele vai inserir mais desta cat1 e quero passar para a próxima assim que ele apanha 1
        inserido = false
        significance_type_2_hash.each_pair { |key,value|
          random = rand(100)
          selected_rest = []

          #para garantir que não vão ser adicionados restaurantes repetidos
          ids_usados = selected_rests_ids.empty? ? 0 : selected_rests_ids.join(",")
          
          if(selected_rests_ids.size < 6 and inserido == false and random <=value)
            selected_rest = Restaurant.joins(:restaurant_categories_of_restaurant).select("id").where("restaurant_id not in (#{ids_usados}) and restaurant_id in (#{rests_of_cat_ids.join(",")}) and restaurant_category_id=#{key}").order("RAND()").limit(1)
            unless selected_rest.empty?
              selected_rests_ids << selected_rest[0].id
              inserido = true
              selected_categories.delete_at(index)
              possible_rests_by_cat.delete_at(index)
            end

          end


        }
        
      }
     
    
    end
  
=begin
    selected_rests_ids.each { |item|

      a = Restaurant.find(item)
      s= a.restaurant_categories.each { |i|
        puts "CATID-->" << i.id.to_s

      }

    }
=end
    #
    # possible_rests_ids

    selected_rests_ids
  end
  #When user_preferences and clicks are OK
  def fuzzy_sugestions(district)
    selected_rests_ids = []

    
    significance_hash = fuzzy(1)
    
    #Check if a category have at least one restaurant for sugest if not, that category should't be added to the prefered cats
    rest_numbers_hash = number_of_restaurants_per_category_type1
    #Select the best categoreis for user of type1
    selected_categories = []
    while selected_categories.size < 6 do

      significance_hash.each { |key,value|

        random_num = rand(100).to_f
        if (random_num < (value *10)  and selected_categories.size < 6 and rest_numbers_hash[key] != nil and rest_numbers_hash[key] > 0)
          logger.info "--------Rand-------->" << random_num.to_s << ".........Significance------->" << (value*10).to_s
          logger.info "--------CAT ID-------->" << key.to_s
          selected_categories << key
          #Isto aqui é para garantir que ele só repete uma categoria caso exista restaurantes suficientes para a poder repetir
          rest_numbers_hash[key] = rest_numbers_hash[key] - 1
        end

      }
    end
    #Categorias preferidas para os 6 rests
    logger.info "Categorias seleccionadas---->" << significance_hash.to_yaml
    logger.info "Categorias seleccionadas---->" << selected_categories.join("::")

    #Ir buscar as categorias de preços

    
    significance_type_2_hash = fuzzy(2)
    
    possible_rests_by_cat = []
    used_ids = []
    #vai Buscar os restaurantes que podem ser possíveis dentro das categorias de comida preferidas
    selected_categories.each { |item|
      #used_string_ids = used_ids.empty? ? 0 : used_ids.join(",")
      rests = Restaurant.joins(:restaurant_categories).select("restaurants.id").where("restaurant_category_id = #{item} and district='#{district}' ").limit(30).order("RAND()")

      #hash de cat_type1 id para array de rests dessa cat
      possible_rests_by_cat << [item, rests]

      rests.each { |i|

        used_ids << i.id
      }
    }

    puts selected_categories.join("---")
    while selected_rests_ids.size < 6 do

      #Depois de escolhidas as categorias do tipo 1 que vão ser recomendadas, vou fazer um array dos restaurantes para cada uma delas
      #para que quando a recomendação baseada na cat2 se poder basear apenas na lista de restaurantes de cat1
      selected_categories.each_with_index { |s_cat, index|
        rests_of_cat_ids = []

        #Isto é para fazer um array dos restaurantes que existem de cada categoria
        possible_rests_by_cat[index][1].each { |item|
          rests_of_cat_ids << item.id

        }
        #quando o gajo é inserido tenho k saltar fora senão ele vai inserir mais desta cat1 e quero passar para a próxima assim que ele apanha 1
        inserido = false
        significance_type_2_hash.each_pair { |key,value|
          random = rand(100)
          selected_rest = []

          #para garantir que não vão ser adicionados restaurantes repetidos
          ids_usados = selected_rests_ids.empty? ? 0 : selected_rests_ids.join(",")

          if(selected_rests_ids.size < 6 and inserido == false and random <=value)
            selected_rest = Restaurant.joins(:restaurant_categories_of_restaurant).select("id").where("restaurant_id not in (#{ids_usados}) and restaurant_id in (#{rests_of_cat_ids.join(",")}) and restaurant_category_id=#{key}").order("RAND()").limit(1)
            unless selected_rest.empty?
              puts s_cat.to_s
              selected_rests_ids << selected_rest[0].id
              inserido = true
              selected_categories.delete_at(index)
              possible_rests_by_cat.delete_at(index)
            end

          end


        }

      }


    end


    selected_rests_ids
  end
 
  #Recives the mode and executes the correspondent method
  def self.sugestions_maker(mode, user)
    logger.debug "MODODDDDDDDDDDDDDDDDDDDDDDDDDDDDDd>>>>>>>>>>>>>>--" << mode.to_s
    case mode
    when 1
      random_sugestions
    when 2
      user.user_sugestions('Lisboa')
    when 3
      user.tracking_sugestions('Lisboa')
    when 4
      user.fuzzy_sugestions('Lisboa')
    end
  end
  
  protected
  # before filter
  def encrypt_password
    return if password.blank?
    self.salt = Digest::SHA1.hexdigest("--#{Time.now.to_s}--#{email}--") if new_record?
    self.password = self.class.encrypt(password, self.salt)
  end



  
end
