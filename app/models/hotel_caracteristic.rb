class HotelCaracteristic < ActiveRecord::Base
  has_and_belongs_to_many :hotel_caracteristics_of_hotels, :join_table => "hotel_caracteristics_of_hotels"
end
