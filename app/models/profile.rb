class Profile < ActiveRecord::Base
  belongs_to :user
  has_attached_file :photo, :styles => { :medium => "300x300>", :thumb => "150x150>", :smaller => "60x60>"}, :allow_nil => true
  validates_attachment_size :photo, :less_then => 5.megabytes, :message=> "O tamanho do ficheiro tem que ser inferior a 5 MegaBytes!"
  validates_attachment_content_type :photo, :content_type=>['image/jpeg','image/png','image/x-png'], :message =>"Tipo de ficheiro não suportado!"

   validates_length_of :home_location, :in => 3..20 , :message=>"A localização só pode ter entre 3 a 20 caracteres!", :on =>"update"
   validates_format_of :home_location, :with => /\A[a-z A-Z]+\z/, :message => "Apenas são permitidas letras na localização!", :on =>"update"
   validates_format_of :phone_number, :with => /\A(?:2|3|9)\d{8}\z/, :message => "Número telefone inválido! Ex:219888888,967777777", :on =>"update"
   
end
