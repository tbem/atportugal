class RestaurantComment < ActiveRecord::Base
  has_one :restaurant
  has_one :user

  def formated_date
    data = self.created_at.to_s
    data2 = data.split(" ")
    data2[0] +" " + data2[1]
  end
end
