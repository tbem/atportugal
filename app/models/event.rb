class Event < ActiveRecord::Base
  has_and_belongs_to_many :event_categories_of_events, :join_table => "event_categories_of_events"
  has_many :event_comments
  has_many :event_photos

end
