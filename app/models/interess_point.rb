class InteressPoint < ActiveRecord::Base
  has_and_belongs_to_many :interess_point_categories_of_interess_points,:join_table => "interess_point_categories_of_interess_points"
  has_many :interess_point_photos
  has_many :interess_point_comments
end
