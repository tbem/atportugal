class RestaurantPhoto < ActiveRecord::Base
  belongs_to  :restaurant
  has_attached_file :restaurant_photo, :styles => { :medium => "450x450>", :thumb=>"150x150>"}
  
  
end
