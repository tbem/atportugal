class InteressPointCategory < ActiveRecord::Base
  has_and_belongs_to_many :interess_point_categories_of_interess_points, :join_table => "interess_point_categories_of_interess_points"
  has_and_belongs_to_many :user_pre_preferences_of_interess_point_categories , :join_table => "user_pre_preferences_of_interess_point_categories"
  has_many :users, :through => :interess_point_categories_of_users
end
