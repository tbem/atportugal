class HotelCategoriesOfUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :hotel_category
end
