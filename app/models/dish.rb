class Dish < ActiveRecord::Base
  belongs_to :menu
  has_many :dish_photos
end
