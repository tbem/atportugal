class UserPrePreference < ActiveRecord::Base
  has_many :user_pre_preferences_of_restaurant_categories
  has_many :restaurant_categories, :through => :user_pre_preferences_of_restaurant_categories

  has_and_belongs_to_many :user_pre_preferences_of_event_categories , :join_table => "user_pre_preferences_of_event_categories"
  has_and_belongs_to_many :user_pre_preferences_of_interess_point_categories , :join_table => "user_pre_preferences_of_interess_point_categories"
  has_and_belongs_to_many :user_pre_preferences_of_hotel_categories, :join_table => "user_pre_preferences_of_hotel_categories"
  belongs_to :user
end
