class EventCategory < ActiveRecord::Base
  has_and_belongs_to_many :event_categories_of_events, :join_table => "event_categories_of_events"
  has_and_belongs_to_many :user_pre_preferences_of_event_categories, :join_table => "user_pre_preferences_of_event_categories"
  has_many :users, :through => :event_categories_of_users
end
