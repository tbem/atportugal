class RestaurantCategoriesOfRestaurant < ActiveRecord::Base
  belongs_to :restaurant
  belongs_to :restaurant_category
  #Isto é para impedir que existam a mesma categoria no mesmo restaurante repetida
  #validates_uniqueness_of :restaurant_id, :scope => :restaurant_category_id
end
