class Hotel < ActiveRecord::Base
  has_and_belongs_to_many :hotel_categories_of_hotels, :join_table => "hotel_categories_of_hotels"
  has_and_belongs_to_many :hotel_caracteristics_of_hotels, :join_table => "hotel_caracteristics_of_hotels"
  has_many :hotel_photos
end
