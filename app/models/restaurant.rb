class Restaurant < ActiveRecord::Base

  has_many :restaurant_caracteristics, :through => :restaurant_caracteristics_of_restaurants
  has_many :restaurant_caracteristics_of_restaurants #ISTO AKI É NECESSARIO PARA O THROW FUNCIONAR
  has_many :restaurant_comments
  has_many :restaurant_photos, :dependent => :destroy
  accepts_nested_attributes_for :restaurant_photos,  :allow_destroy => true
  has_many :menus
  has_many :restaurant_categories_of_restaurant
  has_many :restaurant_categories, :through => :restaurant_categories_of_restaurant

  has_many :restaurant_ratings
  has_many :raters, :through => :restaurant_ratings, :source => :users
  
  has_and_belongs_to_many :closed_days, :join_table => "closed_days_of_restaurants"

  
  has_attached_file :restaurant_logo, :styles => { :medium => "200x200>", :thumb => "100x100>"}, :allow_nil => true
  validates_attachment_size :restaurant_logo, :less_then => 5.megabytes, :message=> "O tamanho do ficheiro tem que ser inferior a 5 MegaBytes!"
  validates_attachment_content_type :restaurant_logo, :content_type=>['image/jpeg','image/png', 'image/x-png'], :message =>"Tipo de ficheiro não suportado!"

  #O KAMINARI SO PAPA SCOPES NO O FIND N FUNCA POR ISSO à K KRIAR UM SCOPE QUE LEVA UM MARAVILHOSO LAMBDA PARA Lhe ESPATAR O D
  scope :advc_search, lambda { |params|
    {
      :include =>[:restaurant_caracteristics, :restaurant_categories],
      :conditions => Restaurant.advanced_search_conditions(params)
    }
  }

  def self.advanced_search_conditions(params = {}) #, limit = nil)
    conditions = []
    categories = []
    categories << "restaurants.id in (select r.id from restaurants r"
    unless params[:restaurant].nil?
     
      unless params[:restaurant][:restaurant_categories][:tipe_1_ids].nil_or_empty?
     
        categories << "inner join restaurant_categories_of_restaurants rcor1 on r.id = rcor1.restaurant_id and rcor1.restaurant_category_id  in (#{params[:restaurant][:restaurant_categories][:tipe_1_ids].join(',')})"
      end
      unless params[:restaurant][:restaurant_categories][:tipe_2_ids].nil_or_empty?
        

        categories << "inner join restaurant_categories_of_restaurants rcor2 on r.id = rcor2.restaurant_id and rcor2.restaurant_category_id in (#{params[:restaurant][:restaurant_categories][:tipe_2_ids].join(',')})"
      end
    end
    categories << ")"
    if (categories.size > 3)
       conditions << categories.join(" ") 
    else
       conditions << categories.join(" ") 
     end
    

    caracteristics_names_values = {}
    caracteristics_names_values["'Zona de Fumadores'"] = "'#{params[:smokers]}'"  if params[:smokers] != "indiferente" and not params[:smokers].nil?
    caracteristics_names_values["'Acesso a pessoas com limitações físicas'"] = "'#{params[:defs]}'" if params[:defs] != "indiferente" and not params[:defs].nil?
    caracteristics_names_values["'Multibanco'"] = "'#{params[:mb]}'"  if params[:mb] != "indiferente" and not params[:mb].nil?
    
    conditions << "restaurant_caracteristics_of_restaurants.caracteristic_value
                  in (#{caracteristics_names_values.values.join(',')})
                  and restaurant_caracteristics.name in (#{caracteristics_names_values.keys.join(',')})
                  and restaurant_caracteristics_of_restaurants.restaurant_caracteristic_id = restaurant_caracteristics.id" unless caracteristics_names_values.empty?

    rest_atributes = []

    
    rest_atributes << "restaurants.name = '#{params[:restaurant_name]}'" unless params[:restaurant_name].nil_or_empty?
    rest_atributes << "restaurants.owner_name = '#{params[:restaurant_owner]}'" unless params[:restaurant_owner].nil_or_empty?
    rest_atributes << "restaurants.district = '#{params[:district]}'" unless params[:district].nil_or_empty?
    rest_atributes << "restaurants.city = '#{params[:city]}'" unless params[:city].nil_or_empty?
    rest_atributes << "restaurants.county = '#{params[:county]}'" unless params[:county].nil_or_empty?
    rest_atributes << "restaurants.location = '#{params[:location]}'" unless params[:location].nil_or_empty?
    rest_atributes << "restaurants.email = '#{params[:email]}'" unless params[:email].nil_or_empty?
    rest_atributes << "restaurants.phone_number = '#{params[:phone_number]}'" unless params[:phone_number].nil_or_empty?

    conditions << rest_atributes.join(" and ") unless rest_atributes.empty?

=begin
    Restaurant.find :all,
      :include =>[:restaurant_caracteristics, :restaurant_categories],
      :conditions => conditions.join(" and "),
      :offset =>offset,
      :limit =>limit
=end
    conditions.join(" and ")

  end

  # === Método para fazer média dos ratings de todos os utilizadores em relação a um restaurante ===
  def average_rating
    value = 0
    
    if self.restaurant_ratings.size > 0
      self.restaurant_ratings.each do |rating|
        r = 0
        if rating.value != nil
          r= rating.value
        end
        value = value + r
      end
      total = self.restaurant_ratings.size
      media = value.to_f / total.to_f
      media = media*4;
      media.to_i
    else
      0
    end
   
  end
end
