class HotelCategory < ActiveRecord::Base
  has_and_belongs_to_many :hotel_categories_of_hotels, :join_table => "hotel_categories_of_hotels"
  has_and_belongs_to_many :user_pre_preferences_of_hotel_categories, :join_table => "user_pre_preferences_of_hotel_categories"
  has_many :users, :through => :hotel_categories_of_users
end
