class ClosedDay < ActiveRecord::Base
  has_and_belongs_to_many :restaurants, :join_table => "closed_days_of_restaurants"
end
