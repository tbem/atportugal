class RestaurantCategory < ActiveRecord::Base
  has_many :restaurant_categories_of_restaurant
  has_many :restaurants, :through => :restaurant_categories_of_restaurant
  
  has_many :user_pre_preferences_of_restaurant_categories
  has_many :user_pre_preferences, :through => :user_pre_preferences_of_restaurant_categories

  has_many :users, :through => :restaurant_categories_of_user

  has_many :restaurant_categories_clicks
  
  validates :name, :uniqueness => true
end
