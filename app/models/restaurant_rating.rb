class RestaurantRating < ActiveRecord::Base

    attr_accessible :value
    belongs_to :restaurant
    belongs_to :user

end
