class RestaurantCategoriesOfUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :restaurant_category
end
