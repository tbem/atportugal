class AddRestaurantDescriptionAndOnwnerColumns < ActiveRecord::Migration
  def self.up
    add_column :restaurants, :owner_name, :string
    add_column :restaurants, :description, :text
  end

  def self.down
    remove_column(:restaurants, :owner_name, :description)
  end
end
