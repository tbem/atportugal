class CreateHotelComments < ActiveRecord::Migration
  def self.up
    create_table :hotel_comments do |t|
 t.references :hotel
      t.text :comment_body
      t.integer :created_by
      t.boolean :active
      t.boolean :visible
      t.timestamps
    end
     execute <<-SQL
      ALTER TABLE hotel_comments
        ADD CONSTRAINT fk_hotel_id_hotel_comments
        FOREIGN KEY (hotel_id)
        REFERENCES hotels(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
       SQL
  end

  def self.down
    drop_table :hotel_comments
  end
end
