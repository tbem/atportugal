class CreateEventPhotos < ActiveRecord::Migration
  def self.up
    create_table :event_photos do |t|
      t.references :event
      t.timestamps
    end
    execute <<-SQL
      ALTER TABLE event_photos
        ADD CONSTRAINT fk_event_photo_id
        FOREIGN KEY (event_id)
        REFERENCES events(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL
  end

  def self.down
    drop_table :event_photos
  end
end
