class CreateUserPrePreferencesOfInteressPoints < ActiveRecord::Migration
  def self.up
    create_table :user_pre_preferences_of_interess_points, :id=>false, :force=>true do |t|
      t.references :user_pre_preference, :interess_point_category
      
    end
     execute <<-SQL
      ALTER TABLE user_pre_preferences_of_interess_points
        ADD CONSTRAINT fk_user_pre_preferences_interess_point_id
        FOREIGN KEY (user_pre_preference_id)
        REFERENCES user_pre_preferences(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL

      execute <<-SQL
      ALTER TABLE user_pre_preferences_of_interess_points
      ADD CONSTRAINT fk_int_point_category_int_point_id
        FOREIGN KEY (interess_point_category_id)
        REFERENCES interess_point_categories(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    SQL
  end

  def self.down
    drop_table :user_pre_preferences_of_interess_points
  end
end
