class CreateRestaurantRatings < ActiveRecord::Migration
  def self.up
    create_table :restaurant_ratings do |t|
      t.integer :user_id
      t.integer :restaurant_id
      t.integer :value

      t.timestamps
    end
  end

  def self.down
    drop_table :restaurant_ratings
  end
end
