class AddValueColumnToUserPrePreferencesOfRestaurantCategories < ActiveRecord::Migration
 def self.up
   add_column :user_pre_preferences_of_restaurant_categories, :significance, :integer
  end

  def self.down
    remove_column(:user_pre_preferences_of_restaurant_categories, :significance)
  end
end
