class AddRestaurantLogoImage < ActiveRecord::Migration
   def self.up
     add_column :restaurants, :restaurant_logo_file_name, :string
    add_column :restaurants, :restaurant_logo_content_type, :string
    add_column :restaurants, :restaurant_logo_file_size, :integer
    add_column :restaurants, :restaurant_logo_updated_at, :datetime
  end

  def self.down
    remove_column :restaurants, :restaurant_logo_file_name
    remove_column :restaurants, :restaurant_logo_content_type
    remove_column :restaurants, :restaurant_logo_file_size
    remove_column :restaurants, :restaurant_logo_updated_at
  end
end
