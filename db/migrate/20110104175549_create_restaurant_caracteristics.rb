class CreateRestaurantCaracteristics < ActiveRecord::Migration
  def self.up
    create_table :restaurant_caracteristics , :force => true do |t|
      t.string :name
      t.string :value
      t.text :description
      t.integer :created_by
      t.boolean :active
      t.boolean :visible
      t.timestamps
    end
    
  end

  def self.down
    drop_table :restaurant_caracteristics
  end
end
