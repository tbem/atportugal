class CreateUserPrePreferencesOfEventCategories < ActiveRecord::Migration
  def self.up
    create_table :user_pre_preferences_of_event_categories, :id=>false, :force=>true do |t|
      t.references :user_pre_preference, :event_category
     
    end
     execute <<-SQL
      ALTER TABLE user_pre_preferences_of_event_categories
        ADD CONSTRAINT fk_user_pre_preferences_event_id
        FOREIGN KEY (user_pre_preference_id)
        REFERENCES user_pre_preferences(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL

      execute <<-SQL
      ALTER TABLE user_pre_preferences_of_event_categories
      ADD CONSTRAINT fk_event_category_event_id
        FOREIGN KEY (event_category_id)
        REFERENCES event_categories(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    SQL
  end

  def self.down
    drop_table :user_pre_preferences_of_event_categories
  end
end
