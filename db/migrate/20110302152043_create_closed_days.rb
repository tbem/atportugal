class CreateClosedDays < ActiveRecord::Migration
  def self.up
    create_table :closed_days do |t|
      t.string :week_day
      t.timestamps
    end
  end

  def self.down
    drop_table :closed_days
  end
end
