class CreateClosedDaysOfRestaurants < ActiveRecord::Migration
  def self.up
     create_table :closed_days_of_restaurants, :id => false, :force => true do |t|
      t.references :closed_day, :restaurant
     end
  end

  def self.down
    drop_table :closed_days_of_restaurants
  end
end
