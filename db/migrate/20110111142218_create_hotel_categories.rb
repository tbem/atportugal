class CreateHotelCategories < ActiveRecord::Migration
  def self.up
    create_table :hotel_categories do |t|
      t.string :name  #Agroturismo
      t.text :description
      t.integer :type #1- Tipo de alojamento , 2- Gama Preço etc...
      t.string :value
      t.integer :created_by
      t.integer :last_updated_by
      t.boolean :active
      t.boolean :visible
      t.timestamps
    end
  end

  def self.down
    drop_table :hotel_categories
  end
end
