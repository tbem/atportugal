class CreateUserPreferences < ActiveRecord::Migration
  def self.up
    create_table :user_preferences, :force=>true do |t|
      t.references :user
      t.timestamps
    end

    execute <<-SQL
      ALTER TABLE user_preferences
        ADD CONSTRAINT fk_user_pref_u_id
        FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL
      
  end

  def self.down
    drop_table :user_preferences
  end
end
