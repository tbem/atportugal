class CreateRolesUsers < ActiveRecord::Migration
  def self.up
     create_table :roles_users, :id => false, :force => true  do |t|
      t.references :user, :role
     
     end
     execute <<-SQL
      ALTER TABLE roles_users
        ADD CONSTRAINT fk_users_id
        FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL
      
      execute <<-SQL
      ALTER TABLE roles_users
      ADD CONSTRAINT fk_roles_id
        FOREIGN KEY (role_id)
        REFERENCES roles(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    SQL
  end

  def self.down
    drop_table :roles_users
  end
end
