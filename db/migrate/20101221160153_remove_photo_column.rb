class RemovePhotoColumn < ActiveRecord::Migration
  def self.up
    remove_column :profiles, :photo
  end

  def self.down
    add_column :profiles, :photo, :string
  end
end
