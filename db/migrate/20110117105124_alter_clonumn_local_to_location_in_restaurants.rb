class AlterClonumnLocalToLocationInRestaurants < ActiveRecord::Migration
  def self.up
    rename_column(:restaurants, :local, :location)
  end

  def self.down
    rename_column(:restaurants, :location, :local)
  end
end
