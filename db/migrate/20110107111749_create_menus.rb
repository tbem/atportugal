class CreateMenus < ActiveRecord::Migration
  def self.up
    create_table :menus, :force=>true do |t|
      t.references :restaurant
      t.string :menu_name
      t.integer :created_by
      t.integer :last_updated_by
      t.boolean :active
      t.boolean :visible
      
      t.timestamps
    end
    execute <<-SQL
      ALTER TABLE menus
        ADD CONSTRAINT fk_rest_menu_id
        FOREIGN KEY (restaurant_id)
        REFERENCES restaurants(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL
  end

  def self.down
    drop_table :menus
  end
end
