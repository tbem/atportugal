class AlterRestarantCategoryTypeColumnName < ActiveRecord::Migration
  def self.up
    rename_column(:restaurant_categories, :type, :restaurant_category_type)
  end

  def self.down
    rename_column(:restaurant_categories,:restaurant_category_type , :type)
  end
end
