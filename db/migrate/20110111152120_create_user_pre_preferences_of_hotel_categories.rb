class CreateUserPrePreferencesOfHotelCategories < ActiveRecord::Migration
  def self.up
    create_table :user_pre_preferences_of_hotel_categories, :id=>false, :force=>true do |t|
      t.references :user_pre_preference, :hotel_category
    end
     execute <<-SQL
      ALTER TABLE user_pre_preferences_of_hotel_categories
        ADD CONSTRAINT fk_user_pre_preferences_hotel_id
        FOREIGN KEY (user_pre_preference_id)
        REFERENCES user_pre_preferences(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL

      execute <<-SQL
      ALTER TABLE user_pre_preferences_of_hotel_categories
      ADD CONSTRAINT fk_hotel_category_hotel_id
        FOREIGN KEY (hotel_category_id)
        REFERENCES hotel_categories(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    SQL
  end

  def self.down
    drop_table :user_pre_preferences_of_hotel_categories
  end
end
