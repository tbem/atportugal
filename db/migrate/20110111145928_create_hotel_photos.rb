class CreateHotelPhotos < ActiveRecord::Migration
  def self.up
    create_table :hotel_photos do |t|
    t.references :hotel

      t.timestamps
    end
    execute <<-SQL
      ALTER TABLE hotel_photos
        ADD CONSTRAINT fk_hotel_photo_id
        FOREIGN KEY (hotel_id)
        REFERENCES hotels(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL
  end

  def self.down
    drop_table :hotel_photos
  end
end
