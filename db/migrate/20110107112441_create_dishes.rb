class CreateDishes < ActiveRecord::Migration
  def self.up
    create_table :dishes do |t|
      t.references :menu
      t.string :name
      t.text :description
      t.float :price
      t.boolean :potluck
      t.integer :created_by
      t.integer :last_updated_by
      t.boolean :active
      t.boolean :visible
      t.timestamps
    end
    execute <<-SQL
      ALTER TABLE dishes
        ADD CONSTRAINT fk_dish_menu_id
        FOREIGN KEY (menu_id)
        REFERENCES menus(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL
  end

  def self.down
    drop_table :dishes
  end
end
