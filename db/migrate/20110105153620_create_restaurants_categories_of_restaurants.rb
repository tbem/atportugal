class CreateRestaurantsCategoriesOfRestaurants < ActiveRecord::Migration
  def self.up
    create_table :restaurants_categories_of_restaurants, :id => false, :force => true do |t|
      t.references :restaurant_category, :restaurant
      
    end
    execute <<-SQL
      ALTER TABLE restaurants_categories_of_restaurants
        ADD CONSTRAINT fk_cat_rest_id
        FOREIGN KEY (restaurant_category_id)
        REFERENCES restaurant_categories(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL

      execute <<-SQL
      ALTER TABLE restaurants_categories_of_restaurants
      ADD CONSTRAINT fk_rest_id
        FOREIGN KEY (restaurant_id)
        REFERENCES restaurants(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    SQL
  end

  def self.down
    drop_table :restaurants_categories_of_restaurants
  end
end
