class CreateEventCategoriesOfUsers < ActiveRecord::Migration
  def self.up
    create_table :event_categories_of_users do |t|
      t.references :event_category, :user
      t.integer :hits
      t.timestamps
    end
    execute <<-SQL
      ALTER TABLE event_categories_of_users
        ADD CONSTRAINT fk_user_pref_envent_id
        FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL

      execute <<-SQL
      ALTER TABLE event_categories_of_users
      ADD CONSTRAINT fk_user_event_cat_id
        FOREIGN KEY (event_category_id)
        REFERENCES event_categories(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    SQL
  end

  def self.down
    drop_table :event_categories_of_users
  end
end
