class CreateInteressPointCategoriesOfUsers < ActiveRecord::Migration
  def self.up
    create_table :interess_point_categories_of_users do |t|
      t.references :interess_point_category, :user
      t.integer :hits
      t.timestamps
    end
     execute <<-SQL
      ALTER TABLE interess_point_categories_of_users
        ADD CONSTRAINT fk_user_pref_int_point_id
        FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL

      execute <<-SQL
      ALTER TABLE interess_point_categories_of_users
      ADD CONSTRAINT fk_user_int_point_cat_id
        FOREIGN KEY (interess_point_category_id)
        REFERENCES interess_point_categories(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    SQL
  end

  def self.down
    drop_table :interess_point_categories_of_users
  end
end
