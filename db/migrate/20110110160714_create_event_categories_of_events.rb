class CreateEventCategoriesOfEvents < ActiveRecord::Migration
  def self.up
    create_table :event_categories_of_events, :id => false, :force => true do |t|
    t.references :event_category, :event
      
    end
    execute <<-SQL
      ALTER TABLE event_categories_of_events
        ADD CONSTRAINT fk_event_cat_event_category_id
        FOREIGN KEY (event_category_id)
        REFERENCES event_categories(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL

      execute <<-SQL
      ALTER TABLE event_categories_of_events
      ADD CONSTRAINT fk_event_id
        FOREIGN KEY (event_id)
        REFERENCES events(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    SQL
  end

  def self.down
    drop_table :event_categories_of_events
  end
end
