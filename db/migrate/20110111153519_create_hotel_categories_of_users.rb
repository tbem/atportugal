class CreateHotelCategoriesOfUsers < ActiveRecord::Migration
  def self.up
    create_table :hotel_categories_of_users do |t|
      t.references :hotel_category, :user
      t.integer :hits
      t.timestamps
    end
    execute <<-SQL
      ALTER TABLE hotel_categories_of_users
        ADD CONSTRAINT fk_user_pref_hotel_id
        FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL

      execute <<-SQL
      ALTER TABLE hotel_categories_of_users
      ADD CONSTRAINT fk_user_hotel_cat_id
        FOREIGN KEY (hotel_category_id)
        REFERENCES hotel_categories(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    SQL
  end

  def self.down
    drop_table :hotel_categories_of_users
  end
end
