class CreateHotelCaracteristics < ActiveRecord::Migration
  def self.up
    create_table :hotel_caracteristics do |t|
      t.string :name
      t.string :value
      t.text :description
      t.integer :created_by
      t.boolean :active
      t.boolean :visible
      t.timestamps
    end
  end

  def self.down
    drop_table :hotel_caracteristics
  end
end
