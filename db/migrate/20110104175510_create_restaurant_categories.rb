class CreateRestaurantCategories < ActiveRecord::Migration
  def self.up
    create_table :restaurant_categories do |t|
      t.string :name
      t.integer :type #1- Gama preço, 2- Tipo Gastronomia
      t.string :value
      t.text :description

      t.timestamps
    end
  end

  def self.down
    drop_table :restaurant_categories
  end
end
