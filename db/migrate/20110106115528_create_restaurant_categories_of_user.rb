class CreateRestaurantCategoriesOfUser < ActiveRecord::Migration
  def self.up
    create_table :restaurant_categories_of_user do |t|
      t.references :restaurant_category, :user
      t.integer :hits
      t.timestamps
    end
    execute <<-SQL
      ALTER TABLE restaurant_categories_of_user
        ADD CONSTRAINT fk_user_pref_rest_id
        FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL

      execute <<-SQL
      ALTER TABLE restaurant_categories_of_user
      ADD CONSTRAINT fk_user_hot_cat_id
        FOREIGN KEY (restaurant_category_id)
        REFERENCES restaurant_categories(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    SQL
  end

  def self.down
    drop_table :user_preferences_of_restaurant_categories
  end
end
