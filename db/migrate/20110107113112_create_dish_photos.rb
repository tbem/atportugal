class CreateDishPhotos < ActiveRecord::Migration
  def self.up
    create_table :dish_photos, :force=>true do |t|
      t.references :dish
      t.integer :created_by
      t.integer :last_updated_by
      t.boolean :active
      t.boolean :visible
      
      t.timestamps
    end
     execute <<-SQL
      ALTER TABLE dish_photos
        ADD CONSTRAINT fk_dish_photo_id
        FOREIGN KEY (dish_id)
        REFERENCES dishes(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL
  end

  def self.down
    drop_table :dish_photos
  end
end
