class CreateInteressPointCategories < ActiveRecord::Migration
  def self.up
    create_table :interess_point_categories do |t|
      t.string :name
      t.text :description
      t.integer :type #1- Praia , 2- Miradouro etc... 
      t.string :value
      t.integer :created_by
      t.integer :last_updated_by
      t.boolean :active
      t.boolean :visible
      t.timestamps
    end
  end

  def self.down
    drop_table :interess_point_categories
  end
end
