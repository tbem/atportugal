class CreateRestaurantCaracteristicsOfRestaurants < ActiveRecord::Migration
  def self.up
    create_table :restaurant_caracteristics_of_restaurants, :force=>true do |t|
    t.references :restaurant_caracteristic, :restaurant
    t.string :caracteristic_value
    end
    execute <<-SQL
      ALTER TABLE restaurant_caracteristics_of_restaurants
        ADD CONSTRAINT fk_restaurant_caracteristic_id
        FOREIGN KEY (restaurant_caracteristic_id)
        REFERENCES restaurant_caracteristics(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL

      execute <<-SQL
      ALTER TABLE restaurant_caracteristics_of_restaurants
      ADD CONSTRAINT fk_restaurant_rest_cat_id
        FOREIGN KEY (restaurant_id)
        REFERENCES restaurants(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    SQL
  end

  def self.down
    drop_table :restaurant_caracteristics_of_restaurants
  end
end
