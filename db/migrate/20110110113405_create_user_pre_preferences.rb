class CreateUserPrePreferences < ActiveRecord::Migration
  def self.up
    create_table :user_pre_preferences do |t|
      t.references :user
      t.timestamps
    end
    execute <<-SQL
      ALTER TABLE user_pre_preferences
        ADD CONSTRAINT fk_user_pre_pref_user_id
        FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL
  end

  def self.down
    drop_table :user_pre_preferences
  end
end
