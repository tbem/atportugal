class CreateEvents < ActiveRecord::Migration
  def self.up
    create_table :events do |t|
      t.string :name
      t.text :description
      t.datetime :start_time
      t.datetime :end_time
      t.time :duration
      t.integer :created_by
      t.integer :last_updated_by
      t.boolean :active
      t.boolean :visible
      
      t.timestamps
    end
  end

  def self.down
    drop_table :events
  end
end

