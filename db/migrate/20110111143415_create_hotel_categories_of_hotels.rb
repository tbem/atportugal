class CreateHotelCategoriesOfHotels < ActiveRecord::Migration
  def self.up
    create_table :hotel_categories_of_hotels, :id =>false, :force=>true do |t|
    t.references :hotel_category, :hotel
      
    end
    execute <<-SQL
      ALTER TABLE hotel_categories_of_hotels
        ADD CONSTRAINT fk_hotel_cat_hotel_category_id
        FOREIGN KEY (hotel_category_id)
        REFERENCES hotel_categories(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL

      execute <<-SQL
      ALTER TABLE hotel_categories_of_hotels
      ADD CONSTRAINT fk_hotel_id
        FOREIGN KEY (hotel_id)
        REFERENCES hotels(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    SQL
  end

  def self.down
    drop_table :hotel_categories_of_hotels
  end
end
