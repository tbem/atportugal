class CreateRestaurantPhotos < ActiveRecord::Migration
  def self.up
    create_table :restaurant_photos do |t|
      t.references :restaurant
      t.timestamps
    end
    execute <<-SQL
      ALTER TABLE restaurant_photos
        ADD CONSTRAINT fk_rest_photo_id
        FOREIGN KEY (restaurant_id)
        REFERENCES restaurants(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL
  end

  def self.down
    drop_table :restaurant_photos
  end
end
