class CreateEventCategories < ActiveRecord::Migration
  def self.up
    create_table :event_categories do |t|
      t.string :name
      t.text :description
      t.integer :type #1- noite , 2- cinema etc... 3-peça teatro, 4-desfile de moda, etc...
      t.string :value
      t.integer :created_by
      t.integer :last_updated_by
      t.boolean :active
      t.boolean :visible
      
      t.timestamps
    end
  end

  def self.down
    drop_table :event_categories
  end
end
