class CreateRestaurantCategoriesClicks < ActiveRecord::Migration
  def self.up
    create_table :restaurant_categories_clicks do |t|
      t.references :user, :restaurant_category
      t.integer :number_of_clicks
      t.timestamps
    end
  end

  def self.down
    drop_table :restaurant_categories_clicks
  end
end
