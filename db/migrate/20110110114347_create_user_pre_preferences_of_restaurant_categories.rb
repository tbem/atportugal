class CreateUserPrePreferencesOfRestaurantCategories < ActiveRecord::Migration
  def self.up
    create_table :user_pre_preferences_of_restaurant_categories, :id=>false, :force=>true do |t|
    t.references :user_pre_preference, :restaurant_category
      
    end
    execute <<-SQL
      ALTER TABLE user_pre_preferences_of_restaurant_categories
        ADD CONSTRAINT fk_user_pre_preferences_rest_id
        FOREIGN KEY (user_pre_preference_id)
        REFERENCES user_pre_preferences(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL

      execute <<-SQL
      ALTER TABLE user_pre_preferences_of_restaurant_categories
      ADD CONSTRAINT fk_restaurant_category_rest_id
        FOREIGN KEY (restaurant_category_id)
        REFERENCES restaurant_categories(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    SQL
  end

  def self.down
    drop_table :user_pre_preferences_of_restaurant_categories
  end
end
