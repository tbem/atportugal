class CreateRestaurants < ActiveRecord::Migration
  def self.up
    create_table :restaurants do |t|
      t.string :name
      t.string :city
      t.string :local
      t.string :district
      t.string :county
      t.text :address
      t.string :postal_code
      t.string :coordinates
      t.string :phone
      t.string :email
      t.integer :phone_number
      t.integer :fax
      t.string :site
      t.timestamps
    end
  end

  def self.down
    drop_table :restaurants
  end
end
