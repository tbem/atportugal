class CreateProfiles < ActiveRecord::Migration
  def self.up
    create_table :profiles, :force => true do |t|
      t.references :user
      t.string :photo
      t.date :birth_date
      t.integer :phone_number
      #t.string :street
      t.string :home_location
      t.string :home_city
      #t.string :postcode
      t.string :profession
      t.string :work_location
      
      t.timestamps
    end
     execute <<-SQL
      ALTER TABLE profiles
        ADD CONSTRAINT fk_users_id_profile
        FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL
    #add_foreign_key(:profiles,:users , :column=>'user_id', :dependent=>:delete)
  end

  def self.down
    drop_table :profiles
  end
end
