class AlterTableNameRestaurantsCategoriesOfRestaurant < ActiveRecord::Migration
   def self.up
        rename_table :restaurants_categories_of_restaurants, :restaurant_categories_of_restaurants
    end
    def self.down
        rename_table :restaurant_categories_of_restaurants, :restaurants_categories_of_restaurants
    end
end
