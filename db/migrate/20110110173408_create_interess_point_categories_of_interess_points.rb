class CreateInteressPointCategoriesOfInteressPoints < ActiveRecord::Migration
  def self.up
    create_table :interess_point_categories_of_interess_points, :id => false, :force => true do |t|
      t.references :interess_point_category, :interess_point
      
    end
    execute <<-SQL
      ALTER TABLE interess_point_categories_of_interess_points
        ADD CONSTRAINT fk_int_point_cat_int_point_category_id
        FOREIGN KEY (interess_point_category_id)
        REFERENCES interess_point_categories(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL

      execute <<-SQL
      ALTER TABLE interess_point_categories_of_interess_points
      ADD CONSTRAINT fk_interess_point_id
        FOREIGN KEY (interess_point_id)
        REFERENCES interess_points(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    SQL
  end

  def self.down
    drop_table :interess_point_categories_of_interess_points
  end
end
