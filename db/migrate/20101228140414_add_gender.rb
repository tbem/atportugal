class AddGender < ActiveRecord::Migration
  def self.up
    add_column :profiles, :gender, :string
  end

  def self.down
    remove_column :profiles, :gender
  end
end
