class CreateRestaurantComments < ActiveRecord::Migration
  def self.up
    create_table :restaurant_comments , :force => true do |t|
      t.references :user
      t.references :restaurant
      t.text :comment_body
      
      t.timestamps


    end

    execute <<-SQL
      ALTER TABLE restaurant_comments
        ADD CONSTRAINT fk_users_id_rest_comments
        FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
        
      SQL

      execute <<-SQL
      ALTER TABLE restaurant_comments
        ADD CONSTRAINT fk_rest_id_rest_comments
        FOREIGN KEY (restaurant_id)
        REFERENCES restaurants(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
       SQL
      
  end

  def self.down
    drop_table :restaurant_comments
  end
end
