class CreateInteressPoints < ActiveRecord::Migration
  def self.up
    create_table :interess_points do |t|
      t.string :name
      t.text :description
      #t.integer :type
      t.integer :created_by
      t.integer :last_updated_by
      t.boolean :active
      t.boolean :visible
      
      t.timestamps
    end
  end

  def self.down
    drop_table :interess_points
  end
end
