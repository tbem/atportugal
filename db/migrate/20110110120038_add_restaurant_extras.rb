class AddRestaurantExtras < ActiveRecord::Migration
  def self.up
    add_column :restaurants, :created_by, :integer
    add_column :restaurants, :last_updated_by, :integer
    add_column :restaurants, :active, :boolean
    add_column :restaurants, :visible, :boolean

  end

  def self.down
    remove_column :restaurants, :created_by
    remove_column :restaurants, :last_updated_by
    remove_column :restaurants, :active
    remove_column :restaurants, :visible
  end

end
