class CreateInteressPointComments < ActiveRecord::Migration
  def self.up
    create_table :interess_point_comments do |t|
      t.references :interess_point
      t.text :comment_body
      t.integer :created_by
      t.boolean :active
      t.boolean :visible
      t.timestamps
    end
    execute <<-SQL
      ALTER TABLE interess_point_comments
        ADD CONSTRAINT fk_interess_point_id_interess_point_comments
        FOREIGN KEY (interess_point_id)
        REFERENCES interess_points(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
       SQL
  end

  def self.down
    drop_table :interess_point_comments
  end
end
