class CreateEventComments < ActiveRecord::Migration
  def self.up
    create_table :event_comments do |t|

      t.references :event
      t.text :comment_body
      t.integer :created_by
      t.boolean :active
      t.boolean :visible
      t.timestamps
    end
    execute <<-SQL
      ALTER TABLE event_comments
        ADD CONSTRAINT fk_event_id_envent_comments
        FOREIGN KEY (event_id)
        REFERENCES events(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
       SQL
  end

  def self.down
    drop_table :event_comments
  end
end
