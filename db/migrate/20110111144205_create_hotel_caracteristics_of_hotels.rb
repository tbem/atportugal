class CreateHotelCaracteristicsOfHotels < ActiveRecord::Migration
  def self.up
    create_table :hotel_caracteristics_of_hotels, :id=>false, :force=>true do |t|
    t.references :hotel_caracteristic, :hotel
      
    end
    execute <<-SQL
      ALTER TABLE hotel_caracteristics_of_hotels
        ADD CONSTRAINT fk_hotel_caracteristic_id
        FOREIGN KEY (hotel_caracteristic_id)
        REFERENCES hotel_caracteristics(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL

      execute <<-SQL
      ALTER TABLE hotel_caracteristics_of_hotels
      ADD CONSTRAINT fk_hotel_hotel_id
        FOREIGN KEY (hotel_id)
        REFERENCES hotels(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    SQL
  end

  def self.down
    drop_table :hotel_caracteristics_of_hotels
  end
end
