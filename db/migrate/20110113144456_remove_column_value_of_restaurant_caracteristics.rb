class RemoveColumnValueOfRestaurantCaracteristics < ActiveRecord::Migration
  def self.up
    remove_column(:restaurant_caracteristics, :value)
  end

  def self.down
    add_column :restaurant_caracteristics, :value, :string
  end
end
