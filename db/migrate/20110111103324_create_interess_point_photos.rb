class CreateInteressPointPhotos < ActiveRecord::Migration
  def self.up
    create_table :interess_point_photos do |t|
      t.references :interess_point
      
      t.timestamps
    end
     execute <<-SQL
      ALTER TABLE interess_point_photos
        ADD CONSTRAINT fk_interess_point_photo_id
        FOREIGN KEY (interess_point_id)
        REFERENCES interess_points(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
      SQL
  end

  def self.down
    drop_table :interess_point_photos
  end
end
