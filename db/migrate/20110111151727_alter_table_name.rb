class AlterTableName < ActiveRecord::Migration
  def self.up
        rename_table :user_pre_preferences_of_interess_points, :user_pre_preferences_of_interess_points_categories
    end
    def self.down
        rename_table :user_pre_preferences_of_interess_points_categories, :user_pre_preferences_of_interess_points
    end
end
