# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20110324142412) do

  create_table "closed_days", :force => true do |t|
    t.string   "week_day"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "closed_days_of_restaurants", :id => false, :force => true do |t|
    t.integer "closed_day_id"
    t.integer "restaurant_id"
  end

  create_table "dish_photos", :force => true do |t|
    t.integer  "dish_id"
    t.integer  "created_by"
    t.integer  "last_updated_by"
    t.boolean  "active"
    t.boolean  "visible"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "dish_photos", ["dish_id"], :name => "fk_dish_photo_id"

  create_table "dishes", :force => true do |t|
    t.integer  "menu_id"
    t.string   "name"
    t.text     "description"
    t.float    "price"
    t.boolean  "potluck"
    t.integer  "created_by"
    t.integer  "last_updated_by"
    t.boolean  "active"
    t.boolean  "visible"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "dishes", ["menu_id"], :name => "fk_dish_menu_id"

  create_table "event_categories", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "type"
    t.string   "value"
    t.integer  "created_by"
    t.integer  "last_updated_by"
    t.boolean  "active"
    t.boolean  "visible"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "event_categories_of_events", :id => false, :force => true do |t|
    t.integer "event_category_id"
    t.integer "event_id"
  end

  add_index "event_categories_of_events", ["event_category_id"], :name => "fk_event_cat_event_category_id"
  add_index "event_categories_of_events", ["event_id"], :name => "fk_event_id"

  create_table "event_categories_of_users", :force => true do |t|
    t.integer  "event_category_id"
    t.integer  "user_id"
    t.integer  "hits"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "event_categories_of_users", ["event_category_id"], :name => "fk_user_event_cat_id"
  add_index "event_categories_of_users", ["user_id"], :name => "fk_user_pref_envent_id"

  create_table "event_comments", :force => true do |t|
    t.integer  "event_id"
    t.text     "comment_body"
    t.integer  "created_by"
    t.boolean  "active"
    t.boolean  "visible"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "event_comments", ["event_id"], :name => "fk_event_id_envent_comments"

  create_table "event_photos", :force => true do |t|
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "event_photos", ["event_id"], :name => "fk_event_photo_id"

  create_table "events", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "start_time"
    t.datetime "end_time"
    t.time     "duration"
    t.integer  "created_by"
    t.integer  "last_updated_by"
    t.boolean  "active"
    t.boolean  "visible"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "hotel_caracteristics", :force => true do |t|
    t.string   "name"
    t.string   "value"
    t.text     "description"
    t.integer  "created_by"
    t.boolean  "active"
    t.boolean  "visible"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "hotel_caracteristics_of_hotels", :id => false, :force => true do |t|
    t.integer "hotel_caracteristic_id"
    t.integer "hotel_id"
  end

  add_index "hotel_caracteristics_of_hotels", ["hotel_caracteristic_id"], :name => "fk_hotel_caracteristic_id"
  add_index "hotel_caracteristics_of_hotels", ["hotel_id"], :name => "fk_hotel_hotel_id"

  create_table "hotel_categories", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "type"
    t.string   "value"
    t.integer  "created_by"
    t.integer  "last_updated_by"
    t.boolean  "active"
    t.boolean  "visible"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "hotel_categories_of_hotels", :id => false, :force => true do |t|
    t.integer "hotel_category_id"
    t.integer "hotel_id"
  end

  add_index "hotel_categories_of_hotels", ["hotel_category_id"], :name => "fk_hotel_cat_hotel_category_id"
  add_index "hotel_categories_of_hotels", ["hotel_id"], :name => "fk_hotel_id"

  create_table "hotel_categories_of_users", :force => true do |t|
    t.integer  "hotel_category_id"
    t.integer  "user_id"
    t.integer  "hits"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "hotel_categories_of_users", ["hotel_category_id"], :name => "fk_user_hotel_cat_id"
  add_index "hotel_categories_of_users", ["user_id"], :name => "fk_user_pref_hotel_id"

  create_table "hotel_comments", :force => true do |t|
    t.integer  "hotel_id"
    t.text     "comment_body"
    t.integer  "created_by"
    t.boolean  "active"
    t.boolean  "visible"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "hotel_comments", ["hotel_id"], :name => "fk_hotel_id_hotel_comments"

  create_table "hotel_photos", :force => true do |t|
    t.integer  "hotel_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "hotel_photos", ["hotel_id"], :name => "fk_hotel_photo_id"

  create_table "hotels", :force => true do |t|
    t.string   "name"
    t.string   "city"
    t.string   "local"
    t.string   "district"
    t.string   "county"
    t.text     "address"
    t.string   "postal_code"
    t.string   "coordinates"
    t.string   "phone"
    t.string   "email"
    t.integer  "phone_number"
    t.integer  "fax"
    t.string   "site"
    t.integer  "created_by"
    t.integer  "last_updated_by"
    t.boolean  "active"
    t.boolean  "visible"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "interess_point_categories", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "type"
    t.string   "value"
    t.integer  "created_by"
    t.integer  "last_updated_by"
    t.boolean  "active"
    t.boolean  "visible"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "interess_point_categories_of_interess_points", :id => false, :force => true do |t|
    t.integer "interess_point_category_id"
    t.integer "interess_point_id"
  end

  add_index "interess_point_categories_of_interess_points", ["interess_point_category_id"], :name => "fk_int_point_cat_int_point_category_id"
  add_index "interess_point_categories_of_interess_points", ["interess_point_id"], :name => "fk_interess_point_id"

  create_table "interess_point_categories_of_users", :force => true do |t|
    t.integer  "interess_point_category_id"
    t.integer  "user_id"
    t.integer  "hits"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "interess_point_categories_of_users", ["interess_point_category_id"], :name => "fk_user_int_point_cat_id"
  add_index "interess_point_categories_of_users", ["user_id"], :name => "fk_user_pref_int_point_id"

  create_table "interess_point_comments", :force => true do |t|
    t.integer  "interess_point_id"
    t.text     "comment_body"
    t.integer  "created_by"
    t.boolean  "active"
    t.boolean  "visible"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "interess_point_comments", ["interess_point_id"], :name => "fk_interess_point_id_interess_point_comments"

  create_table "interess_point_photos", :force => true do |t|
    t.integer  "interess_point_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "interess_point_photos", ["interess_point_id"], :name => "fk_interess_point_photo_id"

  create_table "interess_points", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "created_by"
    t.integer  "last_updated_by"
    t.boolean  "active"
    t.boolean  "visible"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "menus", :force => true do |t|
    t.integer  "restaurant_id"
    t.string   "menu_name"
    t.integer  "created_by"
    t.integer  "last_updated_by"
    t.boolean  "active"
    t.boolean  "visible"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "menus", ["restaurant_id"], :name => "fk_rest_menu_id"

  create_table "password_reset_requests", :force => true do |t|
    t.string   "user_email"
    t.string   "request_hash"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "profiles", :force => true do |t|
    t.integer  "user_id"
    t.date     "birth_date"
    t.integer  "phone_number"
    t.string   "home_location"
    t.string   "home_city"
    t.string   "profession"
    t.string   "work_location"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "gender"
  end

  add_index "profiles", ["user_id"], :name => "fk_users_id_profile"

  create_table "restaurant_caracteristics", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "created_by"
    t.boolean  "active"
    t.boolean  "visible"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "restaurant_caracteristics_of_restaurants", :force => true do |t|
    t.integer "restaurant_caracteristic_id"
    t.integer "restaurant_id"
    t.string  "caracteristic_value"
  end

  add_index "restaurant_caracteristics_of_restaurants", ["restaurant_caracteristic_id"], :name => "fk_restaurant_caracteristic_id"
  add_index "restaurant_caracteristics_of_restaurants", ["restaurant_id"], :name => "fk_restaurant_rest_cat_id"

  create_table "restaurant_categories", :force => true do |t|
    t.string   "name"
    t.integer  "restaurant_category_type"
    t.string   "value"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "restaurant_categories_clicks", :force => true do |t|
    t.integer  "user_id"
    t.integer  "restaurant_category_id"
    t.integer  "number_of_clicks"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "restaurant_categories_of_restaurants", :id => false, :force => true do |t|
    t.integer "restaurant_category_id"
    t.integer "restaurant_id"
  end

  add_index "restaurant_categories_of_restaurants", ["restaurant_category_id"], :name => "fk_cat_rest_id"
  add_index "restaurant_categories_of_restaurants", ["restaurant_id"], :name => "fk_rest_id"

  create_table "restaurant_categories_of_user", :force => true do |t|
    t.integer  "restaurant_category_id"
    t.integer  "user_id"
    t.integer  "hits"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "restaurant_categories_of_user", ["restaurant_category_id"], :name => "fk_user_hot_cat_id"
  add_index "restaurant_categories_of_user", ["user_id"], :name => "fk_user_pref_rest_id"

  create_table "restaurant_comments", :force => true do |t|
    t.integer  "user_id"
    t.integer  "restaurant_id"
    t.text     "comment_body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "restaurant_comments", ["restaurant_id"], :name => "fk_rest_id_rest_comments"
  add_index "restaurant_comments", ["user_id"], :name => "fk_users_id_rest_comments"

  create_table "restaurant_photos", :force => true do |t|
    t.integer  "restaurant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "restaurant_photo_file_name"
    t.string   "restaurant_photo_content_type"
    t.integer  "restaurant_photo_file_size"
    t.datetime "restaurant_photo_updated_at"
  end

  add_index "restaurant_photos", ["restaurant_id"], :name => "fk_rest_photo_id"

  create_table "restaurant_ratings", :force => true do |t|
    t.integer  "user_id"
    t.integer  "restaurant_id"
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "restaurants", :force => true do |t|
    t.string   "name"
    t.string   "city"
    t.string   "location"
    t.string   "district"
    t.string   "county"
    t.text     "address"
    t.string   "postal_code"
    t.string   "coordinates"
    t.string   "email"
    t.integer  "phone_number"
    t.integer  "fax"
    t.string   "site"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "created_by"
    t.integer  "last_updated_by"
    t.boolean  "active"
    t.boolean  "visible"
    t.string   "restaurant_logo_file_name"
    t.string   "restaurant_logo_content_type"
    t.integer  "restaurant_logo_file_size"
    t.datetime "restaurant_logo_updated_at"
    t.string   "owner_name"
    t.text     "description"
  end

  create_table "roles", :force => true do |t|
    t.string   "name",              :limit => 40
    t.string   "authorizable_type", :limit => 40
    t.integer  "authorizable_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles_users", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "roles_users", ["role_id"], :name => "fk_roles_id"
  add_index "roles_users", ["user_id"], :name => "fk_users_id"

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "user_pre_preferences", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_pre_preferences", ["user_id"], :name => "fk_user_pre_pref_user_id"

  create_table "user_pre_preferences_of_event_categories", :id => false, :force => true do |t|
    t.integer "user_pre_preference_id"
    t.integer "event_category_id"
  end

  add_index "user_pre_preferences_of_event_categories", ["event_category_id"], :name => "fk_event_category_event_id"
  add_index "user_pre_preferences_of_event_categories", ["user_pre_preference_id"], :name => "fk_user_pre_preferences_event_id"

  create_table "user_pre_preferences_of_hotel_categories", :id => false, :force => true do |t|
    t.integer "user_pre_preference_id"
    t.integer "hotel_category_id"
  end

  add_index "user_pre_preferences_of_hotel_categories", ["hotel_category_id"], :name => "fk_hotel_category_hotel_id"
  add_index "user_pre_preferences_of_hotel_categories", ["user_pre_preference_id"], :name => "fk_user_pre_preferences_hotel_id"

  create_table "user_pre_preferences_of_interess_points_categories", :id => false, :force => true do |t|
    t.integer "user_pre_preference_id"
    t.integer "interess_point_category_id"
  end

  add_index "user_pre_preferences_of_interess_points_categories", ["interess_point_category_id"], :name => "fk_int_point_category_int_point_id"
  add_index "user_pre_preferences_of_interess_points_categories", ["user_pre_preference_id"], :name => "fk_user_pre_preferences_interess_point_id"

  create_table "user_pre_preferences_of_restaurant_categories", :id => false, :force => true do |t|
    t.integer "user_pre_preference_id"
    t.integer "restaurant_category_id"
    t.integer "significance"
  end

  add_index "user_pre_preferences_of_restaurant_categories", ["restaurant_category_id"], :name => "fk_restaurant_category_rest_id"
  add_index "user_pre_preferences_of_restaurant_categories", ["user_pre_preference_id"], :name => "fk_user_pre_preferences_rest_id"

  create_table "user_preferences", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_preferences", ["user_id"], :name => "fk_user_pref_u_id"

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "name"
    t.string   "last_name"
    t.string   "password"
    t.string   "salt"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
