# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.create(:name => 'Daley', :city => cities.first)
rc = RestaurantCaracteristic.new
rc.name = "Número de lugares sentados"
rc.description = "Número de lugares sentados que o restaurante têm dísponivel para o público."
rc.active = true
rc.visible = true
rc.save
rc.name = "Zona de Fumadores"
rc.active = true
rc.visible = true
rc.save
rc.name = "Multibanco"
rc.description = "Se tem multibanco ou não, este valor tb é booleano!"
rc.active = true
rc.visible = true
rc.save
rc.name = "Dias de Encerramento"
rc.description = "Dias em que o restaurante se encontra fechado!Os valores para dias de encerramento podem ser Seg, Ter, Qua, Qui; Sex, Sab, Dom."
rc.active = true
rc.visible = true
rc.save
rc.name = "Necessidade de Reserva"
rc.description = "Se eventualmente o restaurante tiver frequentemente muita afluência deve ser aconselhavel marcação.Valores:Sim, Não, Aconselhavel."
rc.active = true
rc.visible = true
rc.save

rc.name = "Acesso a pessoas com limitações físicas"
rc.description = "Se o restaurante tem condições para pessoas com deficiências.Valores:booleano"
rc.active = true
rc.visible = true
rc.save

rcat = RestaurantCategory.new
rcat.name = "Chinesa"
rcat.type = 1
rcat.description = "Tipo de Gastronomia"
rcat.save
rcat = RestaurantCategory.new
rcat.name = "Italiana"
rcat.type = 1
rcat.description = "Tipo de Gastronomia"
rcat.save
rcat = RestaurantCategory.new
rcat.name = "Portuguêsa"
rcat.type = 1
rcat.description = "Tipo de Gastronomia"
rcat.save
rcat = RestaurantCategory.new
rcat.name = "Indiana"
rcat.type = 1
rcat.description = "Tipo de Gastronomia"
rcat.save
rcat = RestaurantCategory.new
rcat.name = "Tailandesa"
rcat.type = 1
rcat.description = "Tipo de Gastronomia"
rcat.save
rcat = RestaurantCategory.new
rcat.name = "Japonesa"
rcat.type = 1
rcat.description = "Tipo de Gastronomia"
rcat.save
rcat = RestaurantCategory.new
rcat.name = "Brasileira"
rcat.type = 1
rcat.description = "Tipo de Gastronomia"
rcat.save
rcat = RestaurantCategory.new
rcat.name = "Regional"
rcat.type = 1
rcat.description = "Tipo de Gastronomia"
rcat.save
rcat = RestaurantCategory.new
rcat.name = "Argentina"
rcat.type = 1
rcat.description = "Tipo de Gastronomia"
rcat.save
rcat = RestaurantCategory.new
rcat.name = "Vegetariana"
rcat.type = 1
rcat.description = "Tipo de Gastronomia"
rcat.save
rcat = RestaurantCategory.new
rcat.name = "Mexicana"
rcat.type = 1
rcat.description = "Tipo de Gastronomia"
rcat.save
rcat = RestaurantCategory.new
rcat.name = "Africana"
rcat.type = 1
rcat.description = "Tipo de Gastronomia"
rcat.save
rcat = RestaurantCategory.new
rcat.name = "Marisqueira"
rcat.type = 1
rcat.description = "Tipo de Gastronomia"
rcat.save

#PREÇO MEDIO POR PESSOA
rcat = RestaurantCategory.new
rcat.name = "Inferior a 10 €"
rcat.type = 2
rcat.description = "Preço médio por pessoa"
rcat.save
rcat = RestaurantCategory.new
rcat.name = "Entre 10 € e 20 €"
rcat.type = 2
rcat.description = "Preço médio por pessoa"
rcat.save
rcat = RestaurantCategory.new
rcat.name = "Entre 20 € e 40 €"
rcat.type = 2
rcat.description = "Preço médio por pessoa"
rcat.save
rcat = RestaurantCategory.new
rcat.name = "Entre 40 € e 60 €"
rcat.type = 2
rcat.description = "Preço médio por pessoa"
rcat.save
rcat = RestaurantCategory.new
rcat.name = "Superior a 60 €"
rcat.type = 2
rcat.description = "Preço médio por pessoa"
rcat.save
