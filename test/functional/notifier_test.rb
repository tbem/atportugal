require 'test_helper'

class NotifierTest < ActionMailer::TestCase
  test "send_pass_reset" do
    mail = Notifier.send_pass_reset
    assert_equal "Send pass reset", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
