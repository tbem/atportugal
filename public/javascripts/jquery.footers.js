/*
 * jQuery Footers (based on J.P. Given's Easy Pinned Footer - http://johnpatrickgiven.com)
 * Ivo Jesus, 2010
 */

(function($) {
   $.fn.pinFooter = function(options) {
      // Get the height of the footer and window + window width

      var dH = $(document).height();
      var wH = $(window).height();
      var fw = $(this).outerWidth();
      var fH = $(this).outerHeight(true);

      if (options == 'relative') {
         if (dH <= wH) {
            $(this).css("position","fixed")
                   .css("left","50%")
                   .css("margin-left",(-fw/2)+"px")
                   .css("bottom","0px");
                   //.css("top",wH - fH + "px");
         }
      } else { // Pinned option
         // Set CSS attributes for positioning footer
         var bH = $("body").outerHeight(true);
         var mB = parseInt($("body").css("margin-bottom"));
         $(this).css("position","fixed")
                .css("left","50%")
                .css("margin-left",(-fw/2)+"px")
                .css("top",wH - fH + "px");
         $("body").css("height",(bH + mB) + "px");
      }
   };
})(jQuery);