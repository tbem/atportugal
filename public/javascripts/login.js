function footerDim(){
    bodyHeight = document.body.clientHeight;
    if(bodyHeight  <  $(document).height()-42){
        $("#contents").height($(window).height()-$("#footer").height()-190 +15);//190 do header que n faz falta
        $("#footer").css({
            position: "absolute",
            bottom: "-42px"
        })


    }
    
}
$(document).ready(function(){
    footerDim();
    jQuery('#middle').bind( 'resize', function(e) {
    
        // element's width or height has changed!
   
   
    
        });
           $.fn.wait = function(time, type) {
        time = time || 2000;
        type = type || "fx";
        return this.queue(type, function() {
            var self = this;
            setTimeout(function() {
                $(self).dequeue();
            }, time);
        });
    };


    //document.body.onresize = footerDim();
     
    //CENA PARA CANOS ARREDONDADOS
    //$('#login').mouseover().corner();
    //$('#registo').mouseover().corner();


    //SEARCH PART
    

    //This is for remove de search default text on input focus
    $('#search_input').focus(function(){
        if($('#search_input').val() == "Pesquisar..."){
            $('#search_input').val("")
        }
        
    });
        
    $('#titleAdvancedSearch').corner("5px");

    $.datepicker.regional['pt'] = {
		closeText: 'Fechar',
		prevText: '&#x3c;Anterior',
		nextText: 'Pr&oacute;ximo&#x3e;',
		currentText: 'Hoje',
		monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
		'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
		'Jul','Ago','Set','Out','Nov','Dez'],
		dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
		dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
		dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
		weekHeader: 'Sm',
		dateFormat: 'yy-mm-dd',
		firstDay: 0,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};


    //THIS IS FOR USER DATA CHANGE PARTE
    $('#profile_birth_date').datepicker({
        changeMonth: true,
        changeYear: true,
        showOn: "button",
        buttonImage: "../images/calendar.png",
        buttonImageOnly: true,
        yearRange: '1920:2011',
        showOn: 'both'
    });
    $.datepicker.setDefaults($.datepicker.regional['pt']);
    $( "#profile_birth_date" ).datepicker( "option", "showAnim",'blind' );
    $( "#profile_birth_date" ).datepicker( "option", "dateFormat",'yy-mm-dd');
    $( "#profile_birth_date" ).datepicker( "option", "showButtonPanel", true);
    
    $( "#profile_birth_date" ).datepicker($.datepicker.regional['pt']);

     
    
    $('#profile').toggle(function() {
        

        $('#profileData').animate({
            'height':'show'
        }, {
            duration:'slow'
           
        });
        $('#profile').css('background-position','right 5px');
        $('#contents').css('height','auto');
        $('#footer').css('position','relative');
        $('#footer').removeAttr('style');
    },function(){

        $('#profileData').animate({
            'height':'hide'
        }, {
            duration:'fast'
        });
        $('#profile').css('background-position','right -21px'); //Arrows thing
        footerDim();
    });
    $('#account').toggle(function() {
        $('#contents').css('height','auto');
        $('#footer').css('position','relative');
        $('#footer').removeAttr('style');

        $('#accountData').animate({
            'height':'show'
        }, {
            duration:'slow'

        });
        $('#account').css('background-position','right 5px');
        
    },function(){
   
        $('#accountData').animate({
            'height':'hide'
        }, {
            duration:'fast'
        });
        $('#account').css('background-position','right -21px');
        footerDim();
    });
    $('#preferences').toggle(function() {
       
        $('#preferencesData').animate({
            'height':'show'
        }, {
            duration:'slow'

        });
        $('#preferences').css('background-position','right 5px');
         $('#contents').css('height','auto');
        $('#footer').css('position','relative');
        $('#footer').removeAttr('style');
    },function(){

        $('#preferencesData').animate({
            'height':'hide'
        }, {
            duration:'fast'
        });
        $('#preferences').css('background-position','right -21px');
        footerDim();
    });

    $('.divUserData').corner("5px");

    //THIS IS FOR SUBMIT THE SIMPLE SEARCH FORM
    $('#searchButton').click(function(){
        if($('#search_input').val() != "Pesquisar..." && $('#search_input').val() != "" ){
            $('#simpleSearchForm').submit();
        }else{
            jAlert("Deve de introduzir pelo menos um termo de pesquisa!", "Campo de pesquisa vazio")
        }
        

    });

    //CENA PARA FORM LOGIN
    
    $('#ui_block_div').click(function(){
        
        
        $('#loginForm').animate({
            'height':'hide'
        }, {
            duration:'fast'
        });
        $('#ui_block_div').hide();
        
    });
      
   
   
    $('#login a').toggle(function() {
        $('#ui_block_div').show();
        
        $('#loginForm').animate({
            'height':'show'
        }, {
            duration:'slow',
            easing: 'easeOutBounce'
        });
        
        
    },function(){
        
        $('#loginForm').animate({
            'height':'hide'
        }, {
            duration:'fast'
        });
        $('#ui_block_div').hide();
    });
});
function submitSetPass(){
    $('#resetPassButton').attr("disabled", true);
    $('#wait').show();
    $('#waitimg').show();

    $('#setpass').submit();
}