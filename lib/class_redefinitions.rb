class String
    def nil_or_empty?
      self.empty?
    end
end
class Hash
   def nil_or_empty?
      self.empty?
    end
end
class Array
   def nil_or_empty?
      self.empty?
    end

end
class NilClass
   def nil_or_empty?
      true
    end
end
class Float
  def is_int?
    self == self.to_i.to_f ? true : false
  end
end